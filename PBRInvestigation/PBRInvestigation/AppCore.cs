﻿using SharpDX.Windows;
using SharpDX.DXGI;
using System;
using System.Drawing;
using System.Collections.Generic;

using SharpDX;
using SharpDX.DirectInput;
using D3D11=SharpDX.Direct3D11;

using PBRInvestigation.Camera;
using PBRInvestigation.Device;
using PBRInvestigation.Textures;
using PBRInvestigation.Textures.ResourceViews;
using PBRInvestigation.Scenes;
using PBRInvestigation.Pipeline;
using PBRInvestigation.Pipeline.Pass;

namespace PBRInvestigation
{
    public class AppCore : IDisposable
    {
        private RenderForm mRenderForm;

        private const int mWidth = 1280;
        private const int mHeight = 720;

        private DirectInput mDirectInput;
        private Keyboard mKeyboard;
        private Mouse mMouse;
        private MouseState mPrevMouseState;
        private float mMovementSpeed = 0.01f;
        private float mRotationSpeed = 0.001f;

        private Scene mScene;
        private View mMainView;

        private PipelineInfo mPipelineInfo;
        private RenderPipeline mPipeline;

        private bool mInputEnabled;

        public AppCore()
        {
            mInputEnabled = true;

            mRenderForm = new RenderForm("PBR demo");
            mRenderForm.ClientSize = new Size(mWidth, mHeight);
            mRenderForm.AllowUserResizing = false;

            InitializeInput();

            mPipelineInfo = CreatePipelineInfo();

            mPipeline = new RenderPipeline(mPipelineInfo, mRenderForm.Handle);

            mScene = SceneFactory.CreateFromFile("Assets/Scenes/phong.scene", mPipeline.AssetContainers, mPipeline.DeviceWrapper);

            PopulateRoughnessTexture();

            InitializeView();

            var scenePass = new ScenePass(new SharpDX.Rectangle(0,0, mWidth, mHeight), mPipeline.DeviceWrapper, mPipeline.AssetContainers);

            scenePass.SetScene(mScene);
            scenePass.SetView(mMainView);
            scenePass.RenderTarget = "SceneBuffer";
            scenePass.DepthStencil = "DepthBuffer";

            mPipeline.AddPass(0, scenePass);

            var hdrPass = new HDRPass(new SharpDX.Rectangle(0, 0, 1024, 1024), "BackBuffer", null, mPipeline.DeviceWrapper, mPipeline.AssetContainers);

            mPipeline.AddPass(1, hdrPass);
        }

        private void InitializeInput()
        {
            mDirectInput = new DirectInput();
            mKeyboard = new Keyboard(mDirectInput);
            mMouse = new Mouse(mDirectInput);

            mMouse.Acquire();
            mPrevMouseState = mMouse.GetCurrentState();

            mKeyboard.Properties.BufferSize = 128;
            mKeyboard.Acquire();
        }

        private void InitializeView()
        {
            mMainView = new View(mPipeline.DeviceWrapper, "MainView", mPipeline.AssetContainers);

            Matrix projection = new Matrix();
            Matrix.PerspectiveFovLH((float)Math.PI / 4.0f, (float)mWidth / (float)mHeight, 1.0f, 1000.0f, out projection);
            mMainView.SetProjectionMatrix(projection);

            mMainView.SetViewMatrix(mScene.Camera.CameraMatrix);
        }

        public void Run()
        {
            RenderLoop.Run(mRenderForm, RenderCallback);
        }

        private void RenderCallback()
        {
            mKeyboard.Poll();
            var keyboardState = mKeyboard.GetCurrentState();

            mMouse.Poll();
            var mouseState = mMouse.GetCurrentState();

            if(keyboardState.IsPressed(Key.T))
            {
                mInputEnabled = !mInputEnabled;
            }

            var camera = mScene.Camera as FPSCamera;

            if(mInputEnabled)
            {
                if (keyboardState.IsPressed(Key.W))
                {
                    camera.Walk(mMovementSpeed);
                }

                if (keyboardState.IsPressed(Key.S))
                {
                    camera.Walk(-mMovementSpeed);
                }

                if (keyboardState.IsPressed(Key.A))
                {
                    camera.Strafe(-mMovementSpeed);
                }

                if (keyboardState.IsPressed(Key.D))
                {
                    camera.Strafe(mMovementSpeed);
                }

                if ((mouseState.X != mPrevMouseState.X) || (mouseState.Y != mPrevMouseState.Y))
                {
                    camera.Pitch(mouseState.Y * mRotationSpeed);
                    camera.RotateY(mouseState.X * mRotationSpeed);

                    mPrevMouseState = mouseState;
                }

                if (keyboardState.IsPressed(Key.Up))
                {
                    camera.Pitch(-mRotationSpeed);
                }

                if (keyboardState.IsPressed(Key.Down))
                {
                    camera.Pitch(mRotationSpeed);
                }

                if (keyboardState.IsPressed(Key.Left))
                {
                    camera.RotateY(-mRotationSpeed);
                }

                if (keyboardState.IsPressed(Key.Right))
                {
                    camera.RotateY(mRotationSpeed);
                }
            }

            camera.Update();
            mMainView.SetCameraPos(camera.Position);
            mMainView.SetViewMatrix(camera.CameraMatrix);
            mMainView.UpdateBuffer(mPipeline.DeviceWrapper.ImmediateContext);

            Render();
        }

        private void Render()
        {
            mPipeline.RenderPasses();
        }

        public void Dispose()
        {
            mScene.Dispose();

            mPipeline.Dispose();

            mRenderForm.Dispose();
        }

        private void PopulateRoughnessTexture()
        {
            int LUTSize = 512;

            Texture2D beckmannRoughnessTexture = null;
            if(mPipeline.AssetContainers.GetContainer<Texture2D>().TryGetValue("BeckmannRoughness", out beckmannRoughnessTexture))
            {
                beckmannRoughnessTexture.MapStreamToSubresouce(mPipeline.DeviceWrapper.ImmediateContext, 0, D3D11.MapMode.WriteDiscard, D3D11.MapFlags.None, (mappedStream) =>
                {
                    for (var yIndex = 0; yIndex < LUTSize; yIndex++)
                    {
                        for (var xIndex = 0; xIndex < LUTSize; xIndex++)
                        {
                            float NdotH = xIndex / (float)LUTSize;
                            float Roughness = yIndex / (float)LUTSize;

                            //Normalize the ndoth value
                            NdotH *= 2.0f;
                            NdotH -= 1.0f;

                            float r_sq = Roughness * Roughness;
                            float r_a = 1.0f / (4.0f * r_sq * (float)Math.Pow((double)NdotH, (double)4.0f));
                            float r_b = NdotH * NdotH - 1.0f;
                            float r_c = r_sq * NdotH * NdotH;

                            mappedStream.Write(r_a * (float)Math.Exp(r_b / r_c));
                        }
                    }
                });
            }
        }

        private PipelineInfo CreatePipelineInfo()
        {
            var pipelineInfo = new PipelineInfo();

            var deviceInfo = new DeviceInfo();
            deviceInfo.SwapChain.DriverType = SharpDX.Direct3D.DriverType.Hardware;
            deviceInfo.SwapChain.DeviceCreationFlags = SharpDX.Direct3D11.DeviceCreationFlags.None;

            var backBufferInfo = new BackBufferInfo();
            backBufferInfo.TextureInfo.SourcePath = "BackBuffer";
            backBufferInfo.TextureInfo.ArraySize = 1;
            backBufferInfo.TextureInfo.Format = Format.R8G8B8A8_UNorm;
            backBufferInfo.TextureInfo.BindFlags = new List<SharpDX.Direct3D11.BindFlags> { D3D11.BindFlags.RenderTarget, D3D11.BindFlags.ShaderResource };
            backBufferInfo.TextureInfo.Width = mWidth;
            backBufferInfo.TextureInfo.Height = mHeight;
            backBufferInfo.TextureInfo.SampleDescripiton = new SampleDescription(1, 0);
            backBufferInfo.TextureInfo.ResourceOption = D3D11.ResourceOptionFlags.None;
            backBufferInfo.TextureInfo.Usage = D3D11.ResourceUsage.Default;
            backBufferInfo.TextureInfo.CpuAccsessFlags = D3D11.CpuAccessFlags.None;
            backBufferInfo.TextureInfo.MipLevels = 1;
            backBufferInfo.TextureInfo.RenderTargetViewInfo = new RenderTargetViewInfo(D3D11.RenderTargetViewDimension.Texture2D,
                                                                                       D3D11.DepthStencilViewFlags.None,
                                                                                       Format.R8G8B8A8_UNorm_SRgb);

            backBufferInfo.Usage = new List<Usage> { Usage.BackBuffer, Usage.RenderTargetOutput, Usage.ShaderInput };
            backBufferInfo.RefreshRate = new Rational(60, 1);

            deviceInfo.SwapChain.BackBufferInfo = backBufferInfo;
            deviceInfo.SwapChain.BackBufferCount = 1;
            deviceInfo.SwapChain.IsWindowed = true;

            pipelineInfo.DeviceInfo = deviceInfo;

            var sceneBufferInfo = new TextureInfo(
               "SceneBuffer",
               1,
               new List<SharpDX.Direct3D11.BindFlags>
               {
                    D3D11.BindFlags.RenderTarget,
                    D3D11.BindFlags.ShaderResource
               },
               Format.R16G16B16A16_Float,
               D3D11.ResourceOptionFlags.None,
               D3D11.ResourceUsage.Default,
               D3D11.CpuAccessFlags.None,
               1,
               mWidth,
               mHeight,
               new SampleDescription(1, 0),
               new ShaderResourceViewInfo(SharpDX.Direct3D.ShaderResourceViewDimension.Texture2D, 0, Format.R16G16B16A16_Float),
               null,
               new RenderTargetViewInfo(D3D11.RenderTargetViewDimension.Texture2D, D3D11.DepthStencilViewFlags.None, Format.R16G16B16A16_Float)
            );

            pipelineInfo.Textures.Add(sceneBufferInfo);

            var depthBufferInfo = new TextureInfo(
                "DepthBuffer",
                1,
                new List<SharpDX.Direct3D11.BindFlags>
                {
                    SharpDX.Direct3D11.BindFlags.DepthStencil
                },
                Format.D24_UNorm_S8_UInt,
                D3D11.ResourceOptionFlags.None,
                D3D11.ResourceUsage.Default,
                D3D11.CpuAccessFlags.None,
                1,
                1280,
                720,
                new SampleDescription(1, 0),
                new ShaderResourceViewInfo(SharpDX.Direct3D.ShaderResourceViewDimension.Texture2D, 0, Format.D24_UNorm_S8_UInt),
                new DepthStencilViewInfo(D3D11.DepthStencilViewDimension.Texture2D, D3D11.DepthStencilViewFlags.None, Format.D24_UNorm_S8_UInt),
                null
            );

            pipelineInfo.Textures.Add(depthBufferInfo);

            var beckmannTextureInfo = new TextureInfo(
                "BeckmannRoughness",
                1,
                new List<SharpDX.Direct3D11.BindFlags> { SharpDX.Direct3D11.BindFlags.ShaderResource },
                Format.R32_Float,
                SharpDX.Direct3D11.ResourceOptionFlags.None,
                SharpDX.Direct3D11.ResourceUsage.Dynamic,
                D3D11.CpuAccessFlags.Write,
                1,
                512,
                512,
                new SampleDescription(1, 0),
                new Textures.ResourceViews.ShaderResourceViewInfo(SharpDX.Direct3D.ShaderResourceViewDimension.Texture2D, 0, Format.R32_Float),
                null,
                null
            );

            pipelineInfo.Textures.Add(beckmannTextureInfo);

            var luminanceTexture = new TextureInfo(
                "Luminance",
                1,
                new List<D3D11.BindFlags> { D3D11.BindFlags.RenderTarget, D3D11.BindFlags.ShaderResource },
                Format.R32_Float,
                D3D11.ResourceOptionFlags.GenerateMipMaps,
                D3D11.ResourceUsage.Default,
                D3D11.CpuAccessFlags.None,
                11,
                1024,
                1024,
                new SampleDescription(1, 0),
                new ShaderResourceViewInfo(SharpDX.Direct3D.ShaderResourceViewDimension.Texture2D, 0, Format.R32_Float),
                null,
                new RenderTargetViewInfo(D3D11.RenderTargetViewDimension.Texture2D, D3D11.DepthStencilViewFlags.None, Format.R32_Float)
            );

            pipelineInfo.Textures.Add(luminanceTexture);

            return pipelineInfo;
        }
    }
}
