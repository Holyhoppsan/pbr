﻿using Newtonsoft.Json;
using System.IO;

namespace PBRInvestigation.Serialization
{
    public class JsonSerializer
    {
        public static void Serialize<T>(T type, string path)
        {
            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                using (var writer = new StreamWriter(fileStream))
                {
                    var json = JsonConvert.SerializeObject(type, Formatting.Indented);
                    writer.Write(json);
                }
            }
        }

        public static T Deserialize<T>(string path)
        {
            using (var fileStream = new FileStream(path, FileMode.Open))
            {
                using (var reader = new StreamReader(fileStream))
                {
                    var jsonContent = reader.ReadToEnd();

                    var deserializedObject = JsonConvert.DeserializeObject<T>(jsonContent);
                    return deserializedObject;
                }
            }
        }
    }
}
