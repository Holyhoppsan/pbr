﻿using System;
using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.Buffers.ConstantBuffers
{
    public interface IConstantBuffer : IDisposable
    {
        D3D11.Buffer Buffer { get; }

        void UpdateBuffer(D3D11.DeviceContext deviceContext);
    }
}
