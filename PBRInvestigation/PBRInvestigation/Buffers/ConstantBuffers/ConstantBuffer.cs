﻿using D3D11 = SharpDX.Direct3D11;
using SharpDX;

namespace PBRInvestigation.Buffers.ConstantBuffers
{
    public class ConstantBuffer<T> : IConstantBuffer where T : struct
    { 
        public ConstantBuffer(D3D11.Device device)
        {
            //Check for empty class. in c# 
            if(Utilities.SizeOf<T>() > 1)
            {
                var size = Utilities.SizeOf<T>();
                mBuffer = new D3D11.Buffer(device,
                           Utilities.SizeOf<T>(),
                           D3D11.ResourceUsage.Default,
                           D3D11.BindFlags.ConstantBuffer,
                           D3D11.CpuAccessFlags.None,
                           D3D11.ResourceOptionFlags.None,
                           0);
            }
        }

        public void UpdateBuffer(D3D11.DeviceContext deviceContext)
        {
            //Check for empty class. in c# 
            if (Utilities.SizeOf<T>() > 1)
            {
                deviceContext.UpdateSubresource(ref Data, Buffer);
            }
        }

        public D3D11.Buffer Buffer
        {
            get
            {
                return mBuffer;
            }
        }

        public void Dispose()
        {
            if(mBuffer != null)
            {
                mBuffer.Dispose();
            }
        }

        public T Data;

        private D3D11.Buffer mBuffer;
    }
}
