﻿using PBRInvestigation.Device;

namespace PBRInvestigation.Buffers.ConstantBuffers
{
    public class ConstantBufferFactory
    {
        public static ConstantBuffer<T> Create<T>(DeviceWrapper deviceWrapper) where T : struct
        {
            return new ConstantBuffer<T>(deviceWrapper.Device);
        }
    }
}
