﻿using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Device;
using PBRInvestigation.Container;

namespace PBRInvestigation.Buffers.ConstantBuffers
{
    public class ConstantBufferProxy<T> where T : struct
    {
        public ConstantBufferProxy(string identifier, T initialData, ContainerCollection containerCollection, DeviceWrapper deviceWrapper)
        {
            Data = initialData;
            Identifier = identifier;
            mContainerCollection = containerCollection;

            var buffer = ConstantBufferFactory.Create<T>(deviceWrapper);
            mContainerCollection.GetContainer<IConstantBuffer>().Add(buffer, identifier);
        }

        public void UpdateBuffer(D3D11.DeviceContext deviceContext)
        {
            IConstantBuffer buffer = null;
            if(mContainerCollection.GetContainer<IConstantBuffer>().TryGetValue(Identifier, out buffer))
            {
                var typedBuffer = buffer as ConstantBuffer<T>;

                typedBuffer.Data = Data;
                typedBuffer.UpdateBuffer(deviceContext);
            }         
        }

        public string Identifier { get; private set; }

        public T Data;
        private ContainerCollection mContainerCollection;
    }
}
