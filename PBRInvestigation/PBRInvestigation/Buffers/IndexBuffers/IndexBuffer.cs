﻿using System.Linq;
using SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.Buffers.IndexBuffers
{
    public class IndexBuffer<T> : IIndexBuffer where T : struct
    {
        public IndexBuffer(T[] indices, Format format, D3D11.Device device)
        {
            mIndices = indices;

            mIndexBuffer = D3D11.Buffer.Create<T>(device,
                                                  D3D11.BindFlags.IndexBuffer,
                                                  mIndices);

            mFormat = format;
        }

        public SharpDX.Direct3D11.Buffer Buffer
        {
            get
            {
                return mIndexBuffer;
            }
        }

        public Format Format
        {
            get
            {
                return mFormat;
            }
        }

        public int Count
        {
            get
            {
                return mIndices.Count();
            }
        }

        public void Dispose()
        {
            mIndexBuffer.Dispose();
        }

        private T[] mIndices;
        private D3D11.Buffer mIndexBuffer;
        private Format mFormat;
    }
}
