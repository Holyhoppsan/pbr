﻿using System;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;

namespace PBRInvestigation.Buffers.IndexBuffers
{
    public interface IIndexBuffer: IDisposable
    {
        int Count { get; }
        D3D11.Buffer Buffer { get; }

        DXGI.Format Format { get; }
    }
}
