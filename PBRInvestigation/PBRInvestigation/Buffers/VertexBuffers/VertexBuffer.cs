﻿using D3D11 = SharpDX.Direct3D11;
using SharpDX;


namespace PBRInvestigation.Buffers.VertexBuffers
{
    public class VertexBuffer<T> : IVertexBuffer where T : struct 
    {
        public T[] mVertices;
        private D3D11.Buffer mVertexBuffer;
        private D3D11.VertexBufferBinding mVertexBufferBinding;

        public VertexBuffer(T[] verticies, D3D11.Device device)
        {
            mVertices = verticies;

            mVertexBuffer = D3D11.Buffer.Create<T>(device,
                                                   D3D11.BindFlags.VertexBuffer,
                                                   mVertices);

            var size = Utilities.SizeOf<T>();
            mVertexBufferBinding = new D3D11.VertexBufferBinding(mVertexBuffer, Utilities.SizeOf<T>(), 0);
        }

        public D3D11.VertexBufferBinding Binding
        {
            get
            {
                return mVertexBufferBinding;
            }
        }

        public int Count { get { return mVertices.Length; } }

        public void Dispose()
        {
            mVertexBuffer.Dispose();
        }
    }
}
