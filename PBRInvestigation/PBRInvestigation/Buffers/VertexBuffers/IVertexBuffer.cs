﻿using System;
using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.Buffers.VertexBuffers
{
    public interface IVertexBuffer : IDisposable
    {
        int Count { get; }
        D3D11.VertexBufferBinding Binding { get; }
    }
}
