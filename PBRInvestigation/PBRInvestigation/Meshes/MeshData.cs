﻿using SharpDX;

namespace PBRInvestigation.Meshes
{
    public struct MeshData
    {
        public MeshData(Matrix world)
        {
            WorldMatrix = Matrix.Identity;
            InverseTransposeWorldMatrix = Matrix.Identity;

            SetWorldMatrix(world);
        }

        public void SetWorldMatrix(Matrix world)
        {
            WorldMatrix = world;

            InverseTransposeWorldMatrix = WorldMatrix;
            InverseTransposeWorldMatrix.Column4 = new Vector4(0.0f, 0.0f, 0.0f, 1.0f);

            InverseTransposeWorldMatrix.Invert();
            InverseTransposeWorldMatrix.Transpose();
        }

        Matrix WorldMatrix;
        Matrix InverseTransposeWorldMatrix;
    }
}
