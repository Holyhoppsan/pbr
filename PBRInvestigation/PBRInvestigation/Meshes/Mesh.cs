﻿using PBRInvestigation.Buffers.ConstantBuffers;
using PBRInvestigation.Models.ModelLoader;
using PBRInvestigation.Device;

using PBRInvestigation.Container;

using SharpDX;
using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.Meshes
{
    public class Mesh : IMesh
    {
        public const string NoIndexBuffer = "NoIndexBuffer";

        public Mesh(ModelData modelData, string material, Matrix world, DeviceWrapper deviceWrapper, string identifier, ContainerCollection containerCollection)
        {
            ModelData = modelData;
            Material = material;
            Identifier = identifier;
            Buffer = identifier + "MeshData";
            MeshData = new ConstantBufferProxy<MeshData>(Buffer, new MeshData(world), containerCollection, deviceWrapper);
        }

        public void UpdateBuffer(D3D11.DeviceContext deviceContext)
        {
            MeshData.UpdateBuffer(deviceContext);
        }

        public ModelData ModelData { get;  private set; }

        public string Identifier { get; private set; }

        public string Buffer { get; private set; }

        public string Material { get; set; }

        public ConstantBufferProxy<MeshData> MeshData
        {
            get
            {
                return mMeshBufferProxy;
            }

            set
            {
                mMeshBufferProxy = value;
            }
        }

        public void Dispose()
        {
        }

        private ConstantBufferProxy<MeshData> mMeshBufferProxy;
    }
}
