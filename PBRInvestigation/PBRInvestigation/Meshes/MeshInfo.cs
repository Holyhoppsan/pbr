﻿using SharpDX;

namespace PBRInvestigation.Meshes
{
    public class MeshInfo
    {
        public MeshInfo()
        {

        }

        public MeshInfo(string name, string model, string material, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Name = name;
            Model = model;
            Material = material;
            Position = position;
            Rotation = Rotation;
            Scale = scale;
        }

        public string Name { get; set; }

        public string Model { get; set; }

        public string Material { get; set; }

        public Vector3 Position { get; set; }

        public Quaternion Rotation { get; set; }

        public Vector3 Scale { get; set; }
    }
}
