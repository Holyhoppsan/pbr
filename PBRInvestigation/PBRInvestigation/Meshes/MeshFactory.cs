﻿using PBRInvestigation.Models;
using PBRInvestigation.Models.ModelLoader;
using PBRInvestigation.Materials;
using PBRInvestigation.Device;
using PBRInvestigation.Container;

using SharpDX;

namespace PBRInvestigation.Meshes
{
    public class MeshFactory
    {
        public static IMesh Create(MeshInfo info,
                                    ContainerCollection containerCollection,
                                    DeviceWrapper deviceWrapper)
        {
            ModelData modelData = null;
            if (!containerCollection.GetContainer<ModelData>().TryGetValue(info.Model, out modelData))
            {
                modelData = ModelFactory.CreateFromFile(info.Model, new Models.OBJLoader.OBJModelFormatLoader(containerCollection, deviceWrapper));
                containerCollection.GetContainer<ModelData>().Add(modelData, info.Model);
            }

            if(!containerCollection.GetContainer<IMaterial>().Contains(info.Material))
            {
                var material = MaterialFactory.CreateFromFile(info.Material, containerCollection, deviceWrapper);

                containerCollection.GetContainer<IMaterial>().Add(material, info.Material);
            }

            var worldMatrix = Matrix.Transformation(
                Vector3.Zero,
                Quaternion.Identity,
                info.Scale,
                Vector3.Zero,
                info.Rotation,
                info.Position);

            return new Mesh(modelData, info.Material, worldMatrix, deviceWrapper, info.Name, containerCollection);
        }
    }
}
