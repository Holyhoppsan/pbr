﻿
using SharpDX;

using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Device;
using PBRInvestigation.Textures;
using PBRInvestigation.Buffers.VertexBuffers;
using PBRInvestigation.Buffers.IndexBuffers;
using PBRInvestigation.Buffers.ConstantBuffers;
using PBRInvestigation.Shaders;
using PBRInvestigation.SamplerStates;
using PBRInvestigation.Materials;
using PBRInvestigation.Container;

namespace PBRInvestigation.Meshes
{
    public class ScreenspaceQuad
    {
        public ScreenspaceQuad(string identifier, Rectangle quadRectangle, Rectangle screenRectangle, string material, DeviceWrapper deviceWrapper, ContainerCollection containerCollection)
        {
            mDeviceWrapper = deviceWrapper;

            //Setup the quad mesh.
            var meshInfo = new MeshInfo(identifier, "Assets/Models/screenspacequad.obj", material, Vector3.Zero, Quaternion.Identity, new Vector3(1.0f, 1.0f, 1.0f));

            mQuad = MeshFactory.Create(meshInfo, containerCollection, deviceWrapper);

            mContainerCollection = containerCollection;

            mRectangle = quadRectangle;
            mScreenRectangle = screenRectangle;

            CalculateQuadTransformMatrix();
        }

        public void Render()
        {
            var deviceContext = mDeviceWrapper.ImmediateContext;

            mQuad.UpdateBuffer(deviceContext);

            IConstantBuffer meshBuffer = null;
            mContainerCollection.GetContainer<IConstantBuffer>().TryGetValue(mQuad.Buffer, out meshBuffer);

            foreach (var subModel in mQuad.ModelData.SubModelsIDs)
            {
                IMaterial material = null;
                if (mContainerCollection.GetContainer<IMaterial>().TryGetValue(mQuad.Material, out material))
                {
                    IVertexBuffer vertexBuffer = null;
                    if (mContainerCollection.GetContainer<IVertexBuffer>().TryGetValue(subModel.Item1, out vertexBuffer))
                    {
                        deviceContext.InputAssembler.SetVertexBuffers(0, vertexBuffer.Binding);
                    }

                    IIndexBuffer indexBuffer = null;
                    if (subModel.Item2 != Mesh.NoIndexBuffer)
                    {
                        if (mContainerCollection.GetContainer<IIndexBuffer>().TryGetValue(subModel.Item2, out indexBuffer))
                        {
                            deviceContext.InputAssembler.SetIndexBuffer(indexBuffer.Buffer, indexBuffer.Format, 0);
                        }
                    }

                    deviceContext.InputAssembler.PrimitiveTopology = SharpDX.Direct3D.PrimitiveTopology.TriangleList;

                    //Set vertex shader
                    VertexShader vertexShader = null;
                    if (mContainerCollection.GetContainer<VertexShader>().TryGetValue(material.VertexShaderId, out vertexShader))
                    {
                        deviceContext.VertexShader.Set(vertexShader.Shader);
                        deviceContext.InputAssembler.InputLayout = vertexShader.InputLayout;

                        //Set mesh specific constant buffer (vertex shader)
                        deviceContext.VertexShader.SetConstantBuffer(1, meshBuffer.Buffer);
                    }

                    //Set pixel shader
                    PixelShader pixelShader = null;
                    if (mContainerCollection.GetContainer<PixelShader>().TryGetValue(material.PixelShaderId, out pixelShader))
                    {
                        deviceContext.PixelShader.Set(pixelShader.Shader);
                    }

                    for (int slotIndex = 0; slotIndex < material.TextureSamplerPairs.Count; slotIndex++)
                    {
                        var textureSamplerPair = material.TextureSamplerPairs[slotIndex];

                        Texture2D texture = null;
                        if (mContainerCollection.GetContainer<Texture2D>().TryGetValue(textureSamplerPair.Texture, out texture))
                        {
                            deviceContext.PixelShader.SetShaderResource(slotIndex, texture.ShaderResourceView);
                        }

                        SamplerState samplerState = null;
                        if (mContainerCollection.GetContainer<SamplerState>().TryGetValue(textureSamplerPair.SamplerState, out samplerState))
                        {
                            deviceContext.PixelShader.SetSampler(slotIndex, samplerState.Data);
                        }
                    }

                    D3D11.RasterizerState rasterizerState = null;
                    if (mContainerCollection.GetContainer<D3D11.RasterizerState>().TryGetValue(material.RasterizerStateId, out rasterizerState))
                    {
                        deviceContext.Rasterizer.State = rasterizerState;
                    }

                    D3D11.DepthStencilState depthStencilState = null;
                    if (mContainerCollection.GetContainer<D3D11.DepthStencilState>().TryGetValue(material.DepthStencilStateId, out depthStencilState))
                    {
                        deviceContext.OutputMerger.SetDepthStencilState(depthStencilState);
                    }

                    if (indexBuffer != null)
                    {
                        deviceContext.DrawIndexed(indexBuffer.Count, 0, 0);
                    }
                    else
                    {
                        deviceContext.Draw(vertexBuffer.Count, 0);
                    }

                }
            }
        }

        public Vector2 Size
        {
            get
            {
                return new Vector2(mRectangle.Width, mRectangle.Height);
            }
            set
            {
                mRectangle.Width = (int)value.X;
                mRectangle.Height = (int)value.Y;
                CalculateQuadTransformMatrix();
            }
        }

        public Vector2 Position
        {
            get
            {
                return new Vector2(mRectangle.Left, mRectangle.Top);
            }
            set
            {
                mRectangle.Left = (int)value.X;
                mRectangle.Top = (int)value.Y;
                CalculateQuadTransformMatrix();
            }
        }

        private void CalculateQuadTransformMatrix()
        {
            //Calculate the scale of the quad.
            float widthScale = mRectangle.Width / (float)mScreenRectangle.Width;
            float heightScale = mRectangle.Height / (float)mScreenRectangle.Height;

            var scaleMatrix = Matrix.Scaling(widthScale, heightScale, 1.0f);

            var quadCenterScreenCoord = new Vector3(mRectangle.Left + mRectangle.Width / 2.0f, mRectangle.Top + mRectangle.Height / 2.0f, 0.0f);

            Vector3 upperLeftCornerNDC = PointToNDC(quadCenterScreenCoord);

            var translationMatrix = Matrix.Translation(upperLeftCornerNDC);

            mQuad.MeshData.Data.SetWorldMatrix(scaleMatrix* translationMatrix);
        }

        private Vector3 PointToNDC(Vector3 point)
        {
            return new Vector3(2.0f * point.X / mScreenRectangle.Width - 1.0f, 1.0f - 2.0f*point.Y/ mScreenRectangle.Height, point.Z);
        }

        private DeviceWrapper mDeviceWrapper;

        private ContainerCollection mContainerCollection;

        private IMesh mQuad;

        private Rectangle mRectangle;
        private Rectangle mScreenRectangle;
    }
}
