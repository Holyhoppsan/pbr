﻿using System;

using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Buffers.ConstantBuffers;
using PBRInvestigation.Models.ModelLoader;

namespace PBRInvestigation.Meshes
{
    public interface IMesh : IDisposable
    {
        ModelData ModelData { get; }

        string Material { get; set; }

        string Buffer { get; }

        ConstantBufferProxy<MeshData> MeshData { get; }

        void UpdateBuffer(D3D11.DeviceContext deviceContext);
    }
}
