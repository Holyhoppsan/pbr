﻿using SharpDX;
using D3D11 = SharpDX.Direct3D11;
using PBRInvestigation.Buffers.ConstantBuffers;
using PBRInvestigation.Device;

using PBRInvestigation.Container;

namespace PBRInvestigation
{
    public class View
    { 
        public View(DeviceWrapper deviceWrapper, string identifier, ContainerCollection containerCollection)
        {
            Identifier = identifier;

            mConstantBufferProxy = new ConstantBufferProxy<ViewBufferData>(identifier + "viewbuffer", new ViewBufferData(), containerCollection, deviceWrapper);
        }

        public void SetProjectionMatrix(Matrix projection)
        {
            mConstantBufferProxy.Data.mProjection = projection;
        }

        public void SetViewMatrix(Matrix view)
        {
            mConstantBufferProxy.Data.mView = view;
        }

        public void SetCameraPos(Vector3 position)
        {
            mConstantBufferProxy.Data.mCameraPos = new Vector4(position.X, position.Y, position.Z, 1.0f);
        }

        public void UpdateBuffer(D3D11.DeviceContext deviceContext)
        {
            mConstantBufferProxy.UpdateBuffer(deviceContext);
        }

        public string Identifier { get; private set; }

        public struct ViewBufferData
        {
            public Matrix mProjection;
            public Matrix mView;
            public Vector4 mCameraPos;
        }

        private ConstantBufferProxy<ViewBufferData> mConstantBufferProxy;
    }
}
