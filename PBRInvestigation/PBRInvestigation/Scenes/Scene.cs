﻿using System;
using System.Collections.Generic;

using SharpDX;

using PBRInvestigation.Device;
using PBRInvestigation.Buffers.ConstantBuffers;
using PBRInvestigation.Light;
using PBRInvestigation.Meshes;
using PBRInvestigation.Camera;
using PBRInvestigation.Container;

namespace PBRInvestigation.Scenes
{
    public class Scene : IDisposable
    {
        public Scene(string identifier, DirectionalLight lightSettings, Dictionary<string, IMesh> meshes, Vector3 camerPosition, Vector3 cameraLookAt, Vector3 cameraUp, ContainerCollection containerCollection, DeviceWrapper deviceWrapper)
        {
            Identifier = identifier;
            LightSettings = new ConstantBufferProxy<DirectionalLight>(Identifier + "LightBuffer", lightSettings, containerCollection, deviceWrapper);
            LightSettings.UpdateBuffer(deviceWrapper.ImmediateContext);
            Meshes = meshes;
            Camera = new FPSCamera(camerPosition, cameraLookAt, cameraUp);
        }

        public void Dispose()
        {
            foreach (var mesh in Meshes)
            {
                mesh.Value.Dispose();
            }
        }

        public string Identifier { get; private set; }
        public ConstantBufferProxy<DirectionalLight> LightSettings { get; set; }
        public Dictionary<string, IMesh> Meshes { get; private set; }
        public ICamera Camera { get; private set; }
    }
}
