﻿using System.Linq;

using PBRInvestigation.Device;
using PBRInvestigation.Serialization;
using PBRInvestigation.Meshes;

using PBRInvestigation.Container;

namespace PBRInvestigation.Scenes
{
    public class SceneFactory
    {
        public static Scene CreateFromFile(string path, ContainerCollection containerCollection, DeviceWrapper deviceWrapper)
        {
            var info = JsonSerializer.Deserialize<SceneInfo>(path);
            return Create(path, info, containerCollection, deviceWrapper);
        }

        public static Scene Create(string identifier, SceneInfo info, ContainerCollection containerCollection, DeviceWrapper deviceWrapper)
        { 
            var meshes = info.Meshes.ToDictionary(meshInfo => meshInfo.Name, 
                                                  meshInfo => MeshFactory.Create(meshInfo, containerCollection, deviceWrapper));

            var scene = new Scene(identifier, info.LightSettings, meshes, info.CameraPosition, info.CameraLookAt, info.CameraUp, containerCollection, deviceWrapper);

            return scene;
        }
    }
}
