﻿using System.Collections.Generic;

using SharpDX;

using PBRInvestigation.Meshes;
using PBRInvestigation.Light;

namespace PBRInvestigation.Scenes
{
    public class SceneInfo
    {
        public DirectionalLight LightSettings { get; set; }
        public List<MeshInfo> Meshes { get; set; }

        public Vector3 CameraPosition { get; set; }
        public Vector3 CameraLookAt { get; set; }
        public Vector3 CameraUp { get; set; }

    }
}
