﻿using SharpDX;

namespace PBRInvestigation.Camera
{
    public class FPSCamera : ICamera
    {
        public FPSCamera(Vector3 position, Vector3 lookAt, Vector3 up)
        {
            mCameraMatrix = new Matrix();

            mPosition = position;
            mLookAt = lookAt - position;
            mRight = Vector3.Cross(mUp, mLookAt);
            mUp = up;

            mLookAt.Normalize();
            mUp.Normalize();
            mRight.Normalize();

            Update();
        }

        public Matrix CameraMatrix
        {
            get
            {
                return mCameraMatrix;
            }
        }

        public Vector3 Position
        {
            get
            {
                return mPosition;
            }

            set
            {
                mPosition = value;
            }
        }

        public Vector3 Forward
        {
            get
            {
                return mLookAt;
            }
        }

        public Vector3 Right
        {
            get
            {
                return mRight;
            }
        }

        public Vector3 Up
        {
            get
            {
                return mUp;
            }
        }

        public void Walk(float distance)
        {
            mPosition += mLookAt * distance;
        }

        public void Strafe(float distance)
        {
            mPosition += mRight * distance;
        }

        public void Pitch(float angle)
        {
            var rotationMatrix = Matrix3x3.RotationAxis(mRight, angle);

            mUp = Vector3.Transform(mUp, rotationMatrix);
            mLookAt = Vector3.Transform(mLookAt, rotationMatrix);
        }

        public void RotateY(float angle)
        {
            var rotationMatrix = Matrix3x3.RotationY(angle);

            mUp = Vector3.Transform(mUp, rotationMatrix);
            mLookAt = Vector3.Transform(mLookAt, rotationMatrix);
            mRight = Vector3.Transform(mRight, rotationMatrix);
        }

        public void LookAt(Vector3 position, Vector3 lookAt, Vector3 up)
        {
            Matrix.LookAtLH(ref position, ref lookAt, ref up, out mCameraMatrix);
        }

        public void Update()
        {
            mLookAt.Normalize();
            mRight.Normalize();

            mRight = Vector3.Cross(mUp, mLookAt);

            mUp = Vector3.Cross(mLookAt, mRight);
            mUp.Normalize();        

            float x = -Vector3.Dot(mPosition, mRight);
            float y = -Vector3.Dot(mPosition, mUp);
            float z = -Vector3.Dot(mPosition, mLookAt);

            mCameraMatrix = new Matrix(mRight.X, mUp.X, mLookAt.X, 0,
                                       mRight.Y, mUp.Y, mLookAt.Y, 0,
                                       mRight.Z, mUp.Z, mLookAt.Z, 0,
                                       x, y, z, 1);
        }

        private Vector3 mPosition;
        private Vector3 mLookAt;
        private Vector3 mUp;
        private Vector3 mRight;
        private Matrix mCameraMatrix;
    }
}
