﻿using SharpDX;

namespace PBRInvestigation.Camera
{
    public interface ICamera
    {
        Matrix CameraMatrix { get; }
        Vector3 Position { get; set; }

        Vector3 Forward { get; }
        Vector3 Right { get; }
        Vector3 Up { get; }

        void Update();
    }
}
