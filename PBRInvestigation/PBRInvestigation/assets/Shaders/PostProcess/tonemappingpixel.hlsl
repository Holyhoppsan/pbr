﻿struct PixelIn
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

Texture2D LuminanceTexture : register(t0);
SamplerState LuminanceSampler : register(s0);

Texture2D InputTexture : register(t1);
SamplerState InputSampler : register(s1);

float CalcLuminance(float3 color)
{
	return max(dot(color, float3(0.299f, 0.587f, 0.114f)), 0.0001f);
}

float GetAverageLuminance(float2 texCoord)
{
	return exp(LuminanceTexture.SampleLevel(LuminanceSampler, texCoord, 10.0f).x);
}

float3 CalculateExposedColor(float3 color, float averageLuminance, out float exposure)
{
	exposure = 6.0f;

	return exp2(exposure) * color;
}

float3 TonemapReinhard(float3 color)
{
	float pixelLuminance = CalcLuminance(color);
	float tonemappedLuminance = pixelLuminance / (pixelLuminance + 1.0f);
	return tonemappedLuminance * pow(color / pixelLuminance, 1.0f);
}


float3 Tonemap(float3 color, float averageLuminance, out float exposure)
{
	float3 exposedColor = CalculateExposedColor(color, averageLuminance, exposure);

	return TonemapReinhard(color);
}

float4 main(PixelIn input) : SV_TARGET
{
	float luminance = GetAverageLuminance(input.Tex);
	float3 inputColor = InputTexture.Sample(InputSampler, input.Tex).rgb;

	float exposure = 0;

	float3 tonemappedColor = Tonemap(inputColor, luminance, exposure);

	return float4(tonemappedColor.xyz, 1.0f);
}