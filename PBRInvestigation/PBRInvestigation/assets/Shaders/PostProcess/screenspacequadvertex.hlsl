﻿#include "Assets/Shaders/Common/modelbuffer.hlsl"

struct VertexIn
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

VertexOut main(VertexIn input)
{
	VertexOut output;
	output.Position = mul(WorldMatrix, input.Position);
	output.Tex = input.Tex;
	output.Normal = input.Normal;
	return output;
}