﻿struct PixelIn
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

float CalcLuminance(float3 color)
{
	return max(dot(color, float3(0.299f, 0.587f, 0.114f)), 0.0001f);
}

float4 main(PixelIn input) : SV_TARGET
{
	float3 color = Texture.Sample(Sampler, input.Tex).rgb;

	float luminance = CalcLuminance(color);

	return float4(luminance,1.0f,1.0f, 1.0f);
}