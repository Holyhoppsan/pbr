﻿struct PixelIn
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

float4 main(PixelIn input) : SV_TARGET
{
	return Texture.Sample(Sampler, input.Tex);
}