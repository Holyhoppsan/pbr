﻿struct PixelIn
{
	float4 Position : SV_POSITION;
	float4 LocalPosition : POSITION1;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

cbuffer MaterialData : register(b3)
{
	float4 DiffuseColor;
};

TextureCube SkyTexture : register(t0);
SamplerState Sampler : register(s0);

float4 main(PixelIn input) : SV_TARGET
{
	float4 color = DiffuseColor * SkyTexture.Sample(Sampler, input.LocalPosition);
	return color;
}