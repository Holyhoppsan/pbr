﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/modelbuffer.hlsl"

struct VertexIn
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float4 LocalPosition : POSITION1;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

VertexOut main(VertexIn input)
{
	VertexOut output;
	float4 WorldPos = mul(WorldMatrix, input.Position);
	float4 ViewPos = mul(ViewMatrix, WorldPos);
	output.Position = mul(ProjectionMatrix, ViewPos).xyww;
	output.LocalPosition = input.Position;
	output.Tex = input.Tex;
	output.Normal = mul(WorldInvTransposeMatrix, input.Normal);
	return output;
}