float4 RenderingEquation(in float3 normal, in float3 lightVector, in float4 brdf)
{
	float NDotL = max(0.0f, dot(normalize(normal), -lightVector));

	float4 finalColour = brdf * LightColor * NDotL;

	return float4(finalColour.xyz, 1.0f);
}