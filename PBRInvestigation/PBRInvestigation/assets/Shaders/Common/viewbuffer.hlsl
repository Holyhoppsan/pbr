cbuffer ViewBuffer : register(b0)
{
	float4x4 ProjectionMatrix;
	float4x4 ViewMatrix;
	float4 CameraPosition;
};