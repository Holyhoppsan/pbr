float3 FresnelSchlick(float VDotH, float3 F0)
{
	float3 fresnel = F0 + (1.0f - F0) * pow(1.0f - VDotH, 5.0f);

	return fresnel;
}
