﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/modelbuffer.hlsl"

struct VertexIn
{
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float4 WorldPosition : POSITION0;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

VertexOut main(VertexIn input)
{
	VertexOut output;
	output.WorldPosition = mul(WorldMatrix, input.Position);
	float4 ViewPosition = mul(ViewMatrix, output.WorldPosition);
	output.Position = mul(ProjectionMatrix, ViewPosition);
	output.Tex = input.Tex;
	output.Normal = normalize(mul(WorldInvTransposeMatrix, input.Normal));

	return output;
}