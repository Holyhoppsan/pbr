#include "Assets/Shaders/Common/mathconstants.hlsl"

Texture2D BeckmannLUTTexture : register(t1);
SamplerState BeckmannLUTSampler : register(s1);

float NDFBeckmannLookup(float HDotN, float roughness)
{
	float2 textureCoord = float2(HDotN, roughness);

	textureCoord.x += 1.0f;
	textureCoord.x /= 2.0f;

	return max(0.0, BeckmannLUTTexture.Sample(BeckmannLUTSampler, textureCoord));
}

float NDFGaussian(float3 normal, float3 halfVector, float roughness)
{
	float roughnessSquared = roughness * roughness;

	float c = 1.0f;
	float alpha = acos(dot(normal, halfVector));
	return c * exp(-(alpha / roughnessSquared));
}

float NDFBeckmann(float HDotN, float roughness)
{
	float roughnessSquared = roughness * roughness;
	float HDotNSquared = HDotN * HDotN;
	float a = 1.0f / (4.0f * roughnessSquared * pow(HDotN, 4.0f));
	float b = HDotNSquared - 1.0f;
	float c = roughnessSquared * HDotNSquared;
	return a * exp(b / c);
}

float NDFGGX(float NDotH, float roughness)
{
	float roughnessSquared = roughness * roughness;
	float a2 = roughnessSquared * roughnessSquared;

	float NDotHSquared = NDotH * NDotH;

	float nominator = a2;
	float denominator = (NDotHSquared * (a2 - 1.0f) + 1.0f);
	denominator = denominator * denominator * PI;

	return nominator / denominator;
}
