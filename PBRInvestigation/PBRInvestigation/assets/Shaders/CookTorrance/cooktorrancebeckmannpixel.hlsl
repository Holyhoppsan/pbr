﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/lightbuffer.hlsl"

#include "Assets/Shaders/Common/renderingequation.hlsl"

#include "Assets/Shaders/CookTorrance/fresnelfunctions.hlsl"
#include "Assets/Shaders/CookTorrance/geometryfunctions.hlsl"
#include "Assets/Shaders/CookTorrance/normaldistributionfunctions.hlsl"

cbuffer MaterialData : register(b3)
{
	float4 DiffuseColor;
	float Metallic;
	float Roughness;
};

struct PixelIn
{
	float4 Position : SV_POSITION;
	float4 WorldPosition : POSITION0;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

float4 CookTorranceBRDF(in float3 normal, in float3 viewVector, in float3 lightVector)
{
	float3 normalizedNormal = normalize(normal);

	float3 halfVector = normalize(-lightVector + viewVector);
	float HDotN = max(0.0f, dot(halfVector, normalizedNormal));
	float NDotL = max(0.0f, dot(normalizedNormal, -lightVector));
	float NDotV = max(0.0f, dot(normalizedNormal, viewVector));
	float VDotH = max(0.0f, dot(viewVector, halfVector));

	float3 F0 = lerp(float3(0.04f, 0.04f, 0.04f), DiffuseColor.xyz, Metallic);

	float geometricTerm = GeometryCookTorrance(HDotN, VDotH, NDotV, NDotL);
	float roughnessTerm = NDFBeckmann(HDotN, Roughness);
	float3 fresnelTerm = FresnelSchlick(NDotV, F0);

	float3 reflectionSpecular = (fresnelTerm * geometricTerm * roughnessTerm) / (4 * (NDotV * NDotL) + 0.001f);

	float3 specularTerm = fresnelTerm;
	float3 diffuseTerm = float3(1.0f, 1.0f, 1.0f) - specularTerm;
	diffuseTerm *= 1.0f - Metallic;
	diffuseTerm *= DiffuseColor.xyz;
	diffuseTerm /= PI;
	
	float3 accColor = (reflectionSpecular + diffuseTerm);

	return float4(accColor.xyz, 1.0f);
}

float4 main(PixelIn input) : SV_TARGET
{
	float3 viewVector = normalize(input.WorldPosition - CameraPosition);
	float3 normal = normalize(input.Normal);
	float3 lightDir = normalize(LightDir.xyz);

	float4 cookTorranceBrdf = CookTorranceBRDF(normal, -viewVector, lightDir);
	return RenderingEquation(normal, lightDir, cookTorranceBrdf);
}