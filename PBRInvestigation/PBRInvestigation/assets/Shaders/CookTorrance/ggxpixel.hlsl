﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/lightbuffer.hlsl"

#include "Assets/Shaders/Common/mathconstants.hlsl"

#include "Assets/Shaders/CookTorrance/fresnelfunctions.hlsl"
#include "Assets/Shaders/CookTorrance/geometryfunctions.hlsl"
#include "Assets/Shaders/CookTorrance/normaldistributionfunctions.hlsl"

struct PixelIn
{
	float4 Position : SV_POSITION;
	float4 WorldPosition : POSITION0;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

cbuffer MaterialData : register(b3)
{
	float4 DiffuseColor;
	float Metallic;
	float Roughness;
};

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

TextureCube EnvTexture : register(t1);
SamplerState EnvSampler : register(s1);

float4 CookTorranceBRDF(in float3 normal, in float3 viewVector, in float3 lightVector, in float4 albedo)
{
	float3 normalizedNormal = normalize(normal);

	float3 halfVector = normalize(-lightVector + viewVector);
	float HDotN = max(0.0f, dot(halfVector, normalizedNormal));
	float NDotL = max(0.0f, dot(normalizedNormal, -lightVector));
	float NDotV = max(0.0f, dot(normalizedNormal, viewVector));
	float VDotH = max(0.0f, dot(viewVector, halfVector));

	float3 F0 = lerp(float3(0.04f, 0.04f, 0.04f), albedo.xyz, Metallic);
	float3 fresnelTerm = FresnelSchlick(NDotV, F0);

	float distributionTerm = NDFGGX(HDotN, Roughness);

	float geometryTerm = GeometrySmith(NDotV, NDotL, Roughness);

	float3 brdfNominator = distributionTerm * geometryTerm * fresnelTerm;
	float brdfDenominator = 4 * NDotV * NDotL + 0.001f;
	float3 reflectionSpecular = brdfNominator / brdfDenominator;

	float3 specularTerm = fresnelTerm;
	float3 diffuseTerm = float3(1.0f, 1.0f, 1.0f) - specularTerm;
	diffuseTerm *= 1.0f - Metallic;
	diffuseTerm *= albedo.xyz;
	diffuseTerm /= PI;

	float3 accColor = max(0.0f, NDotL) * (reflectionSpecular + diffuseTerm) * LightColor.xyz;

	return float4(accColor.xyz, 1.0f);
}

float4 main(PixelIn input) : SV_TARGET
{
	float3 viewVector = normalize(input.WorldPosition - CameraPosition);

	float4 diffuseTexture = float4(1.0f, 0.0f, 0.0f, 1.0f);// Texture.Sample(Sampler, input.Tex);
	float4 albedo = DiffuseColor * diffuseTexture;

	return CookTorranceBRDF(normalize(input.Normal), -viewVector, normalize(LightDir.xyz), albedo);
}