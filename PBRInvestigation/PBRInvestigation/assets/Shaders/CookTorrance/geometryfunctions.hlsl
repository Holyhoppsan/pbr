float GeometryCookTorrance(float NDotH, float VDotH, float NDotV, float NDotL)
{
	float numerator = 2.0f * NDotH;
	float denominator = VDotH;

	float b = (numerator * NDotV) / denominator;
	float c = (numerator * NDotL) / denominator;

	return min(b, c);
} 

float GeometrySchlick(float cosineAngle, float roughness)
{
	float r = (roughness + 1.0f);
	float k = (r*r) / 8.0f;

	float nominator = cosineAngle;
	float denominator = cosineAngle * (1.0f - k) + k;

	return nominator / denominator;
}

float GeometrySmith(float NDotV, float NDotL, float roughness)
{
	float masking = GeometrySchlick(NDotV, roughness);
	float shadowing = GeometrySchlick(NDotL, roughness);

	return masking * shadowing;
}