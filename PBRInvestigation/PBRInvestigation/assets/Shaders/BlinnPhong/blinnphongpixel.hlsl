﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/lightbuffer.hlsl"
#include "Assets/Shaders/Common/renderingequation.hlsl"

cbuffer MaterialData : register(b3)
{
	float4 DiffuseColor;
	float4 SpecularColor;
	float SpecularPower;
};

struct PixelIn
{
	float4 Position : SV_POSITION;
	float4 WorldPosition : POSITION0;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

float4 BlinnPhongBRDF(in float3 normal, in float3 viewVector, in float3 lightVector)
{
	float3 halfVector = normalize(-lightVector + viewVector);

	float HDotN = max(0.0f, dot(halfVector, normal));

	float3 specularTerm = SpecularColor.xyz * pow(max(0.0f, HDotN), SpecularPower);

	return float4(DiffuseColor.xyz + specularTerm.xyz, 1.0f);
}

float4 main(PixelIn input) : SV_TARGET
{
	float3 viewVector = normalize(input.WorldPosition - CameraPosition);
	float3 normal = normalize(input.Normal);
	float3 lightDir = normalize(LightDir.xyz);

	float4 blinnPhongBrdf = BlinnPhongBRDF(normal, -viewVector, lightDir);
	return RenderingEquation(normal, lightDir, blinnPhongBrdf);
}