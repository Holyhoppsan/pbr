﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/lightbuffer.hlsl"

#include "Assets/Shaders/Common/renderingequation.hlsl"

cbuffer MaterialData : register(b3)
{
	float4 DiffuseColor;
	float4 SpecularColor;
	float SpecularPower;
};

struct PixelIn
{
	float4 Position : SV_POSITION;
	float4 WorldPosition : POSITION0;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

float4 PhongBRDF(in float3 normal, in float3 viewVector, in float3 lightVector)
{
	float3 reflectionVector = normalize(2.0f * normal * dot(normal, lightVector) - lightVector);

	float RDotV = max(0.0f, dot(reflectionVector, viewVector));

	float3 accSpecular = SpecularColor * pow(RDotV, SpecularPower);

	return float4(DiffuseColor.xyz + accSpecular.xyz, 1.0f);
}

float4 main(PixelIn input) : SV_TARGET
{
	float3 viewVector = normalize(input.WorldPosition - CameraPosition);
	float3 normal = normalize(input.Normal);
	float3 lightDir = normalize(LightDir.xyz);

	float4 phongBrdf = PhongBRDF(normalize(input.Normal), viewVector, lightDir);
	return RenderingEquation(normal, lightDir, phongBrdf);
}