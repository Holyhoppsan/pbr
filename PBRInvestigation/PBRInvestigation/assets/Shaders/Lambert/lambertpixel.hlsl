﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/lightbuffer.hlsl"

#include "Assets/Shaders/Common/renderingequation.hlsl"

cbuffer MaterialData : register(b3)
{
	float4 DiffuseColor;
};

struct PixelIn
{
	float4 Position : SV_POSITION;
	float4 WorldPosition : POSITION0;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
};

float4 LambertBRDF(in float3 normal, in float3 viewVector, in float3 lightVector)
{
	return float4(DiffuseColor.xyz, 1.0f);
}

float4 main(PixelIn input) : SV_TARGET
{
	float3 viewVector = normalize(input.WorldPosition - CameraPosition);
	float3 normal = normalize(input.Normal);
	float3 lightDir = normalize(LightDir.xyz);

	float4 lambertBrdf = LambertBRDF(normal, -viewVector, lightDir);
	return RenderingEquation(normal, lightDir, lambertBrdf);
}