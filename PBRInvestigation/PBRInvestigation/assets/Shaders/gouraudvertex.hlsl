﻿#include "Assets/Shaders/Common/viewbuffer.hlsl"
#include "Assets/Shaders/Common/modelbuffer.hlsl"
#include "Assets/Shaders/Common/lightbuffer.hlsl"

struct VertexIn 
{ 
	float4 Position : POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD; 
};

struct VertexOut
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
	float4 LightColor : COLOR;
};

VertexOut main(VertexIn input)
{
	VertexOut output;
	float4 WorldPos = mul(WorldMatrix, input.Position);
	float4 ViewPos = mul(ViewMatrix, WorldPos);
	output.Position = mul(ProjectionMatrix, ViewPos);
	output.Tex = input.Tex;
	output.Normal = normalize(mul(WorldInvTransposeMatrix, input.Normal));

	//perform vertex gouraud shading
	float NDotL = max(0.0f, dot(output.Normal, -LightDir));
	output.LightColor = float4(NDotL * LightColor.xyz, 1.0f);
	return output;
}