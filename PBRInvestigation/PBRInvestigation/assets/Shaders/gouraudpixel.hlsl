﻿struct PixelIn
{
	float4 Position : SV_POSITION;
	float3 Normal : NORMAL;
	float2 Tex : TEXCOORD;
	float4 LightColor : COLOR;
};

Texture2D Texture : register(t0);
SamplerState Sampler : register(s0);

cbuffer MaterialData : register(b3)
{
	float4 DiffuseColor;
};

float4 main(PixelIn input) : SV_TARGET
{
	float4 diffuseTexture = Texture.Sample(Sampler, input.Tex);
	return diffuseTexture * DiffuseColor * input.LightColor;
}