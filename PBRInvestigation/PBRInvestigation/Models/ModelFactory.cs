﻿using PBRInvestigation.Models.ModelLoader;

namespace PBRInvestigation.Models
{
    public class ModelFactory
    {
        public static ModelData CreateFromFile(string filePath, ModelFormatLoader formatLoader)
        {
            return formatLoader.Load(filePath);
        }
    }
}
