﻿using SharpDX;

namespace PBRInvestigation.Models
{
    public struct VertexPosUV
    {
        public VertexPosUV(Vector3 postion, Vector2 uv)
        {
            Position = postion;
            UV = uv;
        }

        public Vector3 Position { get; private set; }
        public Vector2 UV { get; private set; }
    }
}
