﻿using SharpDX;

namespace PBRInvestigation.Models
{
    public struct VertexPosNormUV
    {
        public VertexPosNormUV(Vector3 postion, Vector3 normal, Vector2 uv)
        {
            Position = postion;
            Normal = normal;
            UV = uv;
        }

        public Vector3 Position { get; private set; }
        public Vector3 Normal { get; private set; }
        public Vector2 UV { get; private set; }
    }
}
