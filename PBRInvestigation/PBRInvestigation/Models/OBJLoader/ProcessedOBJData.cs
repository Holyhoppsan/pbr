﻿using System.Collections.Generic;

namespace PBRInvestigation.Models.OBJLoader
{
    public struct ProcessedOBJData
    {
        public ProcessedOBJData(string name, List<VertexPosNormUV> vertexData, List<uint> indexData)
        {
            Name = name;
            VertexData = vertexData;
            IndexData = indexData;
        }

        public List<VertexPosNormUV> VertexData { get; private set; }
        public List<uint> IndexData { get; private set;  }
        public string Name { get; set; }
    }
}
