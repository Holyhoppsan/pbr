﻿using System.Collections.Generic;
using System.IO;
using SharpDX;

namespace PBRInvestigation.Models.OBJLoader
{
    public class OBJProcessor
    {
        public List<ProcessedOBJData> Process(List<ParsedOBJData> inputDataList)
        {
            var processedOBJDataList = new List<ProcessedOBJData>();
            foreach(var data in inputDataList)
            {
                var vertexData = new List<VertexPosNormUV>();
                var indexData = new List<uint>();

                var existingFaceVertices = new Dictionary<FaceVertex, int>();

                var indexCount = 0;

                foreach (var face in data.Faces)
                {
                    var triangleCount = face.Vertices.Count - 2;
               
                    var firstFaceVertex = face.Vertices[0];
                    var secondFaceVertex = face.Vertices[1];

                    for (var faceVertexIndex = 2; faceVertexIndex < face.Vertices.Count; faceVertexIndex++)
                    {
                        var thirdFaceVertex = face.Vertices[faceVertexIndex];
                        AddVertex(firstFaceVertex, data, ref existingFaceVertices, ref vertexData, ref indexData, ref indexCount);
                        AddVertex(secondFaceVertex, data, ref existingFaceVertices, ref vertexData, ref indexData, ref indexCount);
                        AddVertex(thirdFaceVertex, data, ref existingFaceVertices, ref vertexData, ref indexData, ref indexCount);
                        secondFaceVertex = thirdFaceVertex;
                    }
                }

                processedOBJDataList.Add(new ProcessedOBJData(data.Name, vertexData, indexData));
            }

            return processedOBJDataList;
        }

        private void AddVertex(FaceVertex faceVertex,
                               ParsedOBJData data,
                               ref Dictionary<FaceVertex, int> existingFaceVertices,
                               ref List<VertexPosNormUV> vertexData,
                               ref List<uint> indexData,
                               ref int indexCount)
        {
            if (existingFaceVertices.ContainsKey(faceVertex))
            {
                var existingIndex = 0;

                if (existingFaceVertices.TryGetValue(faceVertex, out existingIndex))
                {
                    indexData.Add((uint)existingIndex);
                }
            }
            else
            {
                AddVertexAndIndexData(faceVertex,
                                      data,
                                      ref existingFaceVertices,
                                      ref vertexData,
                                      ref indexData,
                                      ref indexCount);
            }
        }
 
        private void AddVertexAndIndexData(FaceVertex faceVertex,
                                           ParsedOBJData data,
                                           ref Dictionary<FaceVertex, int> existingFaceVertices,
                                           ref List<VertexPosNormUV> vertexData,
                                           ref List<uint> indexData,
                                           ref int indexCount)
        {
            var positionIndex = GetIndexValue(faceVertex.PositionIndex, data.Positions.Count);
            var vertexPos = data.Positions[positionIndex];

            var texCoordIndex = GetIndexValue(faceVertex.TexCoordIndex, data.TexCoords.Count);
            var texCoord = faceVertex.TexCoordIndex == FaceVertex.UnusedIndex ? new Vector3(0.0f, 0.0f, 0.0f) : data.TexCoords[texCoordIndex];

            var normalIndex = GetIndexValue(faceVertex.NormalIndex, data.Normals.Count);
            var normal = faceVertex.NormalIndex == FaceVertex.UnusedIndex ? new Vector3(0.0f, 0.0f, 0.0f) : data.Normals[normalIndex];

            indexData.Add((uint)vertexData.Count);

            var vertex = new VertexPosNormUV(vertexPos, normal, new SharpDX.Vector2(texCoord.X, texCoord.Y));
            vertexData.Add(vertex);

            existingFaceVertices.Add(faceVertex, indexCount);
            indexCount++;
        }

        private int GetIndexValue(int index, int totalIndexCount)
        {
            return index < 0
                    ? (index + totalIndexCount)
                    : index - 1;
        }
    }
}
