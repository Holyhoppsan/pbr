﻿using System.Collections.Generic;
using SharpDX;

namespace PBRInvestigation.Models.OBJLoader
{
    public class ParsedOBJData
    {
        public ParsedOBJData()
        {
            mVertices = new List<Vector3>();
            mNormals = new List<Vector3>();
            mTexCoords = new List<Vector3>();
            mFaces = new List<Face>();
        }

        public string Name { get; set; }

        public List<Vector3> Positions
        {
            get
            {
                return mVertices;
            }
        }

        public List<Vector3> Normals
        {
            get
            {
                return mNormals;
            }
        }

        public List<Vector3> TexCoords
        {
            get
            {
                return mTexCoords;
            }
        }

        public List<Face> Faces
        {
            get
            {
                return mFaces;
            }
        }

        private List<Vector3> mVertices;
        private List<Vector3> mNormals;
        private List<Vector3> mTexCoords;
        private List<Face> mFaces;
    }
}
