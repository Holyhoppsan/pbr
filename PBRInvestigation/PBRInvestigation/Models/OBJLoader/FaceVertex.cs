﻿using System;

namespace PBRInvestigation.Models.OBJLoader
{
    public class FaceVertex : IEquatable<FaceVertex>
    {
        public const int UnusedIndex = 0;

        public FaceVertex(int posIndex, int normalIndex = UnusedIndex, int texCoordIndex = UnusedIndex)
        {
            mPosNormalTexCoord = new Tuple<int, int, int>(posIndex, normalIndex, texCoordIndex);
        }

        public bool Equals(FaceVertex other)
        {
            if (PositionIndex == other.PositionIndex)
            {
                if (NormalIndex == other.NormalIndex)
                {
                    if (TexCoordIndex == other.TexCoordIndex)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public int PositionIndex
        {
            get
            {
                return mPosNormalTexCoord.Item1;
            }
        }

        public int NormalIndex
        {
            get
            {
                return mPosNormalTexCoord.Item2;
            }
        }

        public int TexCoordIndex
        {
            get
            {
                return mPosNormalTexCoord.Item3;
            }
        }

        public override int GetHashCode()
        {
            return mPosNormalTexCoord.GetHashCode();
        }

        private Tuple<int, int, int> mPosNormalTexCoord;
    }
}
