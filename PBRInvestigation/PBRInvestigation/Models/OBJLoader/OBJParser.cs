﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharpDX;

namespace PBRInvestigation.Models.OBJLoader
{
    public class OBJParser
    {
        public List<ParsedOBJData> ParseOBJData(String input)
        {
            if (input == null)
            {
                throw new ArgumentNullException();
            }

            var parsedObjectDataList = new List<ParsedOBJData>();

            if (input.Length > 0)
            {
                var content = input.Replace('\r', ' ').Split('\n');

                var filteredContent = from line in content
                                      where StartsWithSupportedOBJTag(line)
                                      select line;

                bool previousLineWasFace = false;
                var groupList = new List<string>();
                foreach(var line in filteredContent)
                {
                    var isFace = line.StartsWith("f");
                    if (previousLineWasFace && !isFace)
                    {
                        var parsedOBJData = ParseLines(groupList);
                        parsedObjectDataList.Add(parsedOBJData);
                        groupList.Clear();
                        previousLineWasFace = false;
                    }
                    else if(isFace)
                    {
                        previousLineWasFace = true;
                    }
                    
                    groupList.Add(line);
                }

                if(groupList.Count() != 0)
                {
                    var parsedOBJData = ParseLines(groupList);
                    parsedObjectDataList.Add(parsedOBJData);
                }
            }

            return parsedObjectDataList;
        }

        private bool StartsWithSupportedOBJTag(string line)
        {
            if (line.StartsWith("f") ||
                line.StartsWith("v") ||
                line.StartsWith("vn") ||
                line.StartsWith("vt") ||
                line.StartsWith("g"))
            {
                return true;
            }

            return false;
        }

        private ParsedOBJData ParseLines(IEnumerable<string> input)
        {
            var parsedOBJData = new ParsedOBJData();

            foreach(var line in input)
            {
                var commandSeparatorIndex = line.IndexOf(' ');

                if (commandSeparatorIndex != -1)
                {
                    var tag = line.Substring(0, commandSeparatorIndex);
                    var dataString = line.TrimStart(' ').Substring(commandSeparatorIndex + 1);

                    if (tag == "g")
                    {
                        parsedOBJData.Name = dataString;
                    }
                    else if (tag == "v")
                    {
                        var parsedVertexPosition = ParseVectorString(dataString);
                        parsedOBJData.Positions.Add(parsedVertexPosition);
                    }
                    else if (tag == "vn")
                    {
                        var parsedNormal = ParseVectorString(dataString);
                        parsedOBJData.Normals.Add(parsedNormal);
                    }
                    else if (tag == "vt")
                    {
                        var parsedTexCoord = ParseVectorString(dataString);
                        parsedOBJData.TexCoords.Add(parsedTexCoord);
                    }
                    else if (tag == "f")
                    {
                        var parsedFace = ParseFace(dataString);
                        parsedOBJData.Faces.Add(parsedFace);
                    }
                }
            }

            return parsedOBJData;
        }

        public Vector3 ParseVectorString(String input)
        {
            var stringValues = input.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            switch(stringValues.Count())
            {
                case 2:
                    {
                        return new Vector3(
                            System.Convert.ToSingle(stringValues[0]),
                            System.Convert.ToSingle(stringValues[1]),
                            0.0f
                        );
                    }
                case 3:
                    {
                        return new Vector3(
                            System.Convert.ToSingle(stringValues[0]),
                            System.Convert.ToSingle(stringValues[1]),
                            System.Convert.ToSingle(stringValues[2])
                        );
                    }
                default:
                    throw new ArgumentException("Invalid number of input values for vector");
            }
        }

        public Face ParseFace(String input)
        {
            var face = new Face();

            if (input.Count() > 0)
            {
                var faceVertexStrings = input.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                foreach (var faceVertexString in faceVertexStrings)
                {
                    face.Vertices.Add(ParseFaceVertexString(faceVertexString));
                }
            }

            return face;
        }

        public FaceVertex ParseFaceVertexString(String input)
        {
            var indexStrings = input.Replace("//", "/").Split('/');

            switch (indexStrings.Count())
            {
                case 1:
                    {
                        return new FaceVertex(System.Convert.ToInt32(indexStrings[0]));
                    }
                case 2:
                    {
                        if (input.Contains("//"))
                        {
                            return new FaceVertex(
                                System.Convert.ToInt32(indexStrings[0]),
                                System.Convert.ToInt32(indexStrings[1])
                            );
                        }
                        else
                        {
                            return new FaceVertex(
                                System.Convert.ToInt32(indexStrings[0]),
                                FaceVertex.UnusedIndex,
                                System.Convert.ToInt32(indexStrings[1])
                            );
                        }
                    }
                case 3:
                    {
                        return new FaceVertex(
                                System.Convert.ToInt32(indexStrings[0]),
                                System.Convert.ToInt32(indexStrings[2]),
                                System.Convert.ToInt32(indexStrings[1])
                        );
                    }
                default:
                    {
                        return new FaceVertex(FaceVertex.UnusedIndex);
                    }
            }
        }
    }
}
