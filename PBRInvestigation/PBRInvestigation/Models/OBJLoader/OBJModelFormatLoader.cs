﻿using System;
using System.Collections.Generic;
using System.IO;

using SharpDX.DXGI;

using PBRInvestigation.Device;
using PBRInvestigation.Buffers.VertexBuffers;
using PBRInvestigation.Buffers.IndexBuffers;
using PBRInvestigation.Models.ModelLoader;
using PBRInvestigation.Container;

namespace PBRInvestigation.Models.OBJLoader
{
    public class OBJModelFormatLoader : ModelFormatLoader
    {
        public OBJModelFormatLoader(ContainerCollection containerCollection,
                                    DeviceWrapper deviceWrapper) 
            : base(containerCollection, deviceWrapper)
        {
            mParser = new OBJParser();
            mProcessor = new OBJProcessor();
        }

        public override ModelData Load(string filePath)
        {
            using (var fileStream = new FileStream(filePath, FileMode.Open))
            {
                using (var objContents = new StreamReader(fileStream))
                {
                    var parsedData = mParser.ParseOBJData(objContents.ReadToEnd());

                    var processedData = mProcessor.Process(parsedData);

                    var subModelList = new List<Tuple<string, string>>();
                    foreach(var subModel in processedData)
                    {
                        var vertexBufferName = subModel.Name + "_vertexbuffer";
                        ContainerCollection.GetContainer<IVertexBuffer>().Add(new VertexBuffer<VertexPosNormUV>(subModel.VertexData.ToArray(), mDeviceWrapper.Device), vertexBufferName);

                        var indexBufferName = subModel.Name + "_indexbuffer";
                        ContainerCollection.GetContainer<IIndexBuffer>().Add(new IndexBuffer<uint>(subModel.IndexData.ToArray(), Format.R32_UInt, mDeviceWrapper.Device), indexBufferName);

                        subModelList.Add(new Tuple<string, string>(vertexBufferName, indexBufferName));
                    }

                    return new ModelData(subModelList);
                }
            }  
        }

        private OBJParser mParser;
        private OBJProcessor mProcessor;
    }
}
