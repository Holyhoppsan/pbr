﻿using System.Collections.Generic;

namespace PBRInvestigation.Models.OBJLoader
{
    public class Face
    {
        private List<FaceVertex> mVertices;

        public Face()
        {
            mVertices = new List<FaceVertex>();
        }

        public List<FaceVertex> Vertices
        {
            get
            {
                return mVertices;
            }
        }
    }
}
