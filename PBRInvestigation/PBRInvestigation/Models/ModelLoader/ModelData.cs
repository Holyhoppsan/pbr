﻿using System;
using System.Collections.Generic;

namespace PBRInvestigation.Models.ModelLoader
{
    public class ModelData : IDisposable
    {
        public ModelData(List<Tuple<string, string>> subModelIDs)
        {
            SubModelsIDs = subModelIDs;
        }

        public void Dispose()
        {
        }

        public List<Tuple<string, string>> SubModelsIDs { get; private set; }
    }
}
