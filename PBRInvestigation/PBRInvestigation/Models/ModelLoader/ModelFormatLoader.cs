﻿using PBRInvestigation.Buffers.VertexBuffers;
using PBRInvestigation.Buffers.IndexBuffers;
using PBRInvestigation.Device;
using PBRInvestigation.Container;

namespace PBRInvestigation.Models.ModelLoader
{
    public abstract class ModelFormatLoader
    {
        protected ModelFormatLoader(ContainerCollection containerCollection,
                                    DeviceWrapper deviceWrapper)
        {
            ContainerCollection = containerCollection;
            mDeviceWrapper = deviceWrapper;
        }

        public abstract ModelData Load(string filePath);

        protected ContainerCollection ContainerCollection { get; set; }
        protected DeviceWrapper mDeviceWrapper;
    }
}
