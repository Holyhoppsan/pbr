﻿using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Device;
using PBRInvestigation.Serialization;

namespace PBRInvestigation.RasterizerState
{
    public class RasterizerStateFactory
    {
        public static D3D11.RasterizerState CreateFromFile(string path, DeviceWrapper deviceWrapper)
        {
            var info = JsonSerializer.Deserialize<RasterizerStateInfo>(path);

            return Create(info, deviceWrapper);
        }

        public static D3D11.RasterizerState Create(RasterizerStateInfo info, DeviceWrapper deviceWrapper)
        {
            var description = new D3D11.RasterizerStateDescription();

            description.CullMode = info.CullMode;
            description.DepthBias = info.DepthBias;
            description.DepthBiasClamp = info.DepthBiasClamp;
            description.FillMode = info.FillMode;
            description.IsAntialiasedLineEnabled = info.IsAntialiasedLineEnabled;
            description.IsDepthClipEnabled = info.IsDepthClipEnabled;
            description.IsFrontCounterClockwise = info.IsFrontCounterClockwise;
            description.IsMultisampleEnabled = info.IsMultisampleEnabled;
            description.IsScissorEnabled = info.IsScissorEnabled;
            description.SlopeScaledDepthBias = info.SlopeScaledDepthBias;

            return new SharpDX.Direct3D11.RasterizerState(deviceWrapper.Device, description);
        }
        
    }
}
