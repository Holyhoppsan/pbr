﻿using D3D11 = SharpDX.Direct3D11;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PBRInvestigation.RasterizerState
{
    public class RasterizerStateInfo
    {
        public RasterizerStateInfo()
        {

        }

        public RasterizerStateInfo(D3D11.CullMode cullMode,
                                   int depthBias,
                                   float depthBiasClamp,
                                   D3D11.FillMode fillMode,
                                   bool isAntialiasedLineEnabled,
                                   bool isDepthClipEnabled,
                                   bool isFrontCounterClockwise,
                                   bool isMultisampleEnabled,
                                   bool isScissorEnabled,
                                   float slopeScaledDepthBias)
        {
            CullMode = cullMode;
            DepthBias = depthBias;
            DepthBiasClamp = depthBiasClamp;
            FillMode = FillMode;
            IsAntialiasedLineEnabled = isAntialiasedLineEnabled;
            IsDepthClipEnabled = isDepthClipEnabled;
            IsFrontCounterClockwise = isFrontCounterClockwise;
            IsMultisampleEnabled = isMultisampleEnabled;
            IsScissorEnabled = isScissorEnabled;
            SlopeScaledDepthBias = slopeScaledDepthBias;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.CullMode CullMode { get; set; }

        public int DepthBias { get; set; }

        public float DepthBiasClamp { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.FillMode FillMode { get; set; }

        public bool IsAntialiasedLineEnabled { get; set; }

        public bool IsDepthClipEnabled { get; set; }

        public bool IsFrontCounterClockwise { get; set; }

        public bool IsMultisampleEnabled { get; set; }

        public bool IsScissorEnabled { get; set; }

        public float SlopeScaledDepthBias { get; set; }
    }
}
