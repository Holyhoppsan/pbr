﻿using System;

namespace PBRInvestigation
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            System.Console.WriteLine("PBR Demo started");
            using (var appCore = new AppCore())
            {
                appCore.Run();
            }
            System.Console.WriteLine("PBR Demo shutdown");
        }
    }
}
