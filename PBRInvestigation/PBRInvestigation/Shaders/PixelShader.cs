﻿using System;
using D3D11 = SharpDX.Direct3D11;
using D3DCompiler = SharpDX.D3DCompiler;

namespace PBRInvestigation.Shaders
{
    public class PixelShader : IDisposable
    {
        public PixelShader(string shaderFilePath, D3D11.Device device)
        {
            using (var pixelShaderByteCode = D3DCompiler.ShaderBytecode.CompileFromFile(shaderFilePath, 
                                                                                        "main", 
                                                                                        "ps_5_0", 
                                                                                         D3DCompiler.ShaderFlags.Debug,
                                                                                         D3DCompiler.EffectFlags.None,
                                                                                         null,
                                                                                         new ShaderInclude()))
            {
                Console.WriteLine(String.Format("---- Compiling Shader {0} ----", shaderFilePath));
                Console.Write(pixelShaderByteCode.Message);

                mPixelShader = new D3D11.PixelShader(device, pixelShaderByteCode);
            }
        }

        public D3D11.PixelShader Shader { get { return mPixelShader; } }

        public void Dispose()
        {
            mPixelShader.Dispose();
        }

        private D3D11.PixelShader mPixelShader;
    }
}
