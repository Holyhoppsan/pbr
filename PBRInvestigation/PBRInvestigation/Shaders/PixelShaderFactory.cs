﻿using PBRInvestigation.Device;

namespace PBRInvestigation.Shaders
{
    public class PixelShaderFactory
    {
        public static PixelShader CreateFromFile(string path, DeviceWrapper deviceWrapper)
        {
            try
            {
                return new PixelShader(path, deviceWrapper.Device);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Exception occured when loading pixel shader: " + e.Message);
                return null;
            }
        }
    }
}
