﻿using System;
using System.IO;

using D3DCompiler = SharpDX.D3DCompiler;

namespace PBRInvestigation.Shaders
{
    public class ShaderInclude : D3DCompiler.Include
    {
        public IDisposable Shadow
        {
            get;
            set;
        }

        public void Close(Stream stream)
        {
            stream.Dispose();
        }

        public void Dispose()
        {
            Shadow.Dispose();
        }

        public Stream Open(D3DCompiler.IncludeType type, string fileName, Stream parentStream)
        {
            string path = Directory.GetCurrentDirectory();
            return new FileStream(fileName, FileMode.Open);
        }
    }
}
