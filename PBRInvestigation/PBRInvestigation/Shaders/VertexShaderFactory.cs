﻿using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Device;

namespace PBRInvestigation.Shaders
{
    public class VertexShaderFactory
    {
        public static VertexShader CreateFromFile(string path, D3D11.InputElement[] inputElements, DeviceWrapper deviceWrapper)
        {
            try
            {
                return new VertexShader(path, inputElements, deviceWrapper.Device);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Exception occured when loading vertex shader: " + e.Message);
                return null;
            }
        }
    }
}
