﻿using System;
using D3D11 = SharpDX.Direct3D11;

using D3DCompiler = SharpDX.D3DCompiler;


namespace PBRInvestigation.Shaders
{
    public class VertexShader : IDisposable
    {
        public VertexShader(string shaderFilePath, D3D11.InputElement[] inputElements, D3D11.Device device)
        {
            using (var vertexShaderByteCode = D3DCompiler.ShaderBytecode.CompileFromFile(shaderFilePath, 
                                                                                         "main", 
                                                                                         "vs_5_0", 
                                                                                         D3DCompiler.ShaderFlags.Debug,
                                                                                         D3DCompiler.EffectFlags.None,
                                                                                         null,
                                                                                         new ShaderInclude()))
            {
                Console.WriteLine(String.Format("---- Compiling Shader {0} ----", shaderFilePath));
                Console.Write(vertexShaderByteCode.Message);

                mVertexShader = new D3D11.VertexShader(device, vertexShaderByteCode);
                mInputSignature = D3DCompiler.ShaderSignature.GetInputSignature(vertexShaderByteCode);
            }

            mInputLayout = new D3D11.InputLayout(device, mInputSignature, inputElements);
        }

        public D3D11.VertexShader Shader { get { return mVertexShader; } }

        public D3D11.InputLayout InputLayout { get { return mInputLayout; } }

        public void Dispose()
        {
            mInputLayout.Dispose();
            mInputSignature.Dispose();
            mVertexShader.Dispose();
        }

        private D3D11.VertexShader mVertexShader;
        private D3D11.InputLayout mInputLayout;
        private D3DCompiler.ShaderSignature mInputSignature;
    }
}
