﻿using System;
using System.Collections.Generic;

using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.Materials
{
    public struct TextureSamplerPair
    {
        public string Texture;
        public string SamplerState;
    }

    public interface IMaterial : IDisposable
    {
        string PixelShaderId { get; }
        string VertexShaderId { get; }
        IList<TextureSamplerPair> TextureSamplerPairs { get; }
        string RasterizerStateId { get; }
        string DepthStencilStateId { get; }
        string BufferId { get; }

        void AddTextureSamplerPair(string texture, string sampler);

        void SetTexture(int slot, string texture);
        void SetSampler(int slot, string sampler);

        void UpdateBuffer(D3D11.DeviceContext deviceContext);
    }
}
