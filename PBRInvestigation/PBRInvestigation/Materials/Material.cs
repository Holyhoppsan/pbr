﻿using System.Collections.Generic;

using PBRInvestigation.Device;
using PBRInvestigation.Buffers.ConstantBuffers;
using SharpDX.Direct3D11;
using PBRInvestigation.Container;

namespace PBRInvestigation.Materials
{
    public class MaterialHeader
    {
        public MaterialHeader()
        {

        }

        public MaterialHeader(string vertexShader,
                              string pixelShader,
                              IList<TextureSamplerPair> textureSamplerPairs,
                              string rasterizerState,
                              string depthStencilState)
        {
            PixelShader = pixelShader;
            VertexShader = vertexShader;
            TextureSamplerPairs = textureSamplerPairs;
            RasterizerState = rasterizerState;
            DepthStencilState = depthStencilState;
        }

        public string PixelShader { get; set; }
        public string VertexShader { get; set; }

        public IList<TextureSamplerPair> TextureSamplerPairs { get; set; }

        public string RasterizerState { get; set; }

        public string DepthStencilState { get; set; }
    }

    public class Material<BufferData> : IMaterial where BufferData : struct 
    {
        public Material(string identifier,
                        MaterialHeader header, 
                        BufferData data,
                        ContainerCollection containerCollection, 
                        DeviceWrapper deviceWrapper)
        {
            mHeaderData = header;
            BufferId = identifier;
            BufferDataProxy = new ConstantBufferProxy<BufferData>(identifier, data, containerCollection, deviceWrapper);
        }

        public string PixelShaderId { get { return mHeaderData.PixelShader; } }
        public string VertexShaderId { get { return mHeaderData.VertexShader; } }
        public IList<TextureSamplerPair> TextureSamplerPairs { get { return mHeaderData.TextureSamplerPairs; } }
        public string RasterizerStateId { get { return mHeaderData.RasterizerState; } }
        public string DepthStencilStateId { get { return mHeaderData.DepthStencilState; } }

        public string BufferId { get; private set; }

        public ConstantBufferProxy<BufferData> BufferDataProxy { get; private set; }

        public void Dispose()
        {
        }

        public void AddTextureSamplerPair(string texture, string sampler)
        {
            var pair = new TextureSamplerPair();
            pair.Texture = texture;
            pair.SamplerState = sampler;
            mHeaderData.TextureSamplerPairs.Add(pair);
        }

        public void SetTexture(int slot, string texture)
        {
            var textureSamplerPair = mHeaderData.TextureSamplerPairs[slot];
            textureSamplerPair.Texture = texture;
            mHeaderData.TextureSamplerPairs[slot] = textureSamplerPair;
        }

        public void SetSampler(int slot, string sampler)
        {
            var textureSamplerPair = mHeaderData.TextureSamplerPairs[slot];
            textureSamplerPair.SamplerState = sampler;
            mHeaderData.TextureSamplerPairs[slot] = textureSamplerPair;
        }

        public void UpdateBuffer(DeviceContext deviceContext)
        {
            BufferDataProxy.UpdateBuffer(deviceContext);
        }

        private MaterialHeader mHeaderData;
    }
}
