﻿using System.Runtime.InteropServices;
using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.Materials.Constants
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CookTorranceConstants
    {
        public RawColor4 DiffuseColor { get; set; }
        public float Metallic { get; set; }
        public float Roughness { get; set; }
        private RawVector2 Padding;
    }
}
