﻿using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.Materials.Constants
{
    public struct LambertConstants
    {
        public RawColor4 DiffuseColor { get; set; }
    }
}
