﻿using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.Materials.Constants
{
    public struct FlatConstants
    {
        public RawColor4 DiffuseColor { get; set; }
    }
}
