﻿using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.Materials.Constants
{
    public struct GouraudConstants
    {
        public RawColor4 DiffuseColor { get; set; }
    }
}
