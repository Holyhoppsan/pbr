﻿using System.Runtime.InteropServices;
using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.Materials.Constants
{
    [StructLayout(LayoutKind.Sequential)]
    public struct PhongConstants
    {
        public RawColor4 DiffuseColor { get; set; }
        public RawColor4 SpecularColor { get; set; }
        public float SpecularPower { get; set; }
        private RawVector3 Padding;
    }
}
