﻿using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.Materials.Constants
{
    public struct SkyBoxConstants
    {
        public RawColor4 DiffuseColor { get; set; }
    }
}
