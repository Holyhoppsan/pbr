﻿using System.IO;

using DXGI = SharpDX.DXGI;
using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Shaders;
using PBRInvestigation.Textures;
using PBRInvestigation.SamplerStates;
using PBRInvestigation.RasterizerState;
using PBRInvestigation.DepthStencilState;
using PBRInvestigation.Device;
using PBRInvestigation.Container;
using PBRInvestigation.Materials.Constants;

using Newtonsoft.Json.Linq;

namespace PBRInvestigation.Materials
{
    public class MaterialFactory
    {
        public static IMaterial CreateFromFile(string path, ContainerCollection containerCollection, DeviceWrapper deviceWrapper)
        {
            using (var fileStream = new FileStream(path, FileMode.Open))
            {
                using (var reader = new StreamReader(fileStream))
                {
                    var jsonContent = reader.ReadToEnd();
                    return CreateFromJson(path, jsonContent, containerCollection, deviceWrapper);
                }
            }
        }

        public static IMaterial Create<T>(string identifier, MaterialHeader header, T constants, ContainerCollection containerCollection, DeviceWrapper deviceWrapper) where T : struct
        {
            var material = new Material<T>(identifier, header, constants, containerCollection, deviceWrapper);

            material.UpdateBuffer(deviceWrapper.ImmediateContext);
            LoadMaterialDependencies(material, containerCollection, deviceWrapper);

            return material;
        }


        private static IMaterial CreateFromJson(string identifier, string jsonContent, ContainerCollection containerCollection, DeviceWrapper deviceWrapper)
        {
            IMaterial material = null;

            var fullParsedData = JObject.Parse(jsonContent);

            var materialType = fullParsedData["MaterialType"].Value<string>();
            var materialHeaderContent = fullParsedData["MaterialHeader"];
            var materialConstants = fullParsedData["Constants"];

            var materialHeader = materialHeaderContent.ToObject<MaterialHeader>();

            switch (materialType)
            {
                case "Lambert":
                    {
                        var lambertConstants = materialConstants.ToObject<LambertConstants>();
                        material = Create(identifier, materialHeader, lambertConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "Flat":
                    {
                        var flatConstants = materialConstants.ToObject<FlatConstants>();
                        material = Create(identifier, materialHeader, flatConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "Gouraud":
                    {
                        var gouraudConstants = materialConstants.ToObject<GouraudConstants>();
                        material = Create(identifier, materialHeader, gouraudConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "Phong":
                    {
                        var phongConstants = materialConstants.ToObject<PhongConstants>();
                        material = Create(identifier, materialHeader, phongConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "BlinnPhong":
                    {
                        var blinnPhongConstants = materialConstants.ToObject<BlinnPhongConstants>();
                        material = Create(identifier, materialHeader, blinnPhongConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "Skybox":
                    {
                        var skyBoxConstants = materialConstants.ToObject<SkyBoxConstants>();
                        material = Create(identifier, materialHeader, skyBoxConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "CookTorrance":
                    {
                        var cookTorranceConstants = materialConstants.ToObject<CookTorranceConstants>();
                        material = Create(identifier, materialHeader, cookTorranceConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "Resolve":
                    {
                        var resolveConstants = materialConstants.ToObject<ResolveConstants>();
                        material = Create(identifier, materialHeader, resolveConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "Luminance":
                    {
                        var luminanceConstants = materialConstants.ToObject<LuminanceConstants>();
                        material = Create(identifier, materialHeader, luminanceConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "Tonemapping":
                    {
                        var tonemappingConstants = materialConstants.ToObject<TonemappingConstants>();
                        material = Create(identifier, materialHeader, tonemappingConstants, containerCollection, deviceWrapper);
                    }
                    break;
                case "GGX":
                    {
                        var ggxConstants = materialConstants.ToObject<GGXConstants>();
                        material = Create(identifier, materialHeader, ggxConstants, containerCollection, deviceWrapper);
                    }
                    break;
            }

            return material;
        }

        private static void LoadMaterialDependencies(IMaterial material, ContainerCollection containerCollection, DeviceWrapper deviceWrapper)
        {
            if (!containerCollection.GetContainer<VertexShader>().Contains(material.VertexShaderId))
            {
                var vertexShader = VertexShaderFactory.CreateFromFile(material.VertexShaderId,
                                                                      new D3D11.InputElement[] { new D3D11.InputElement("POSITION", 0, DXGI.Format.R32G32B32_Float, 0, 0),
                                                                      new D3D11.InputElement("NORMAL", 0, DXGI.Format.R32G32B32_Float, 12, 0),
                                                                      new D3D11.InputElement("TEXCOORD",0, DXGI.Format.R32G32_Float, 24, 0)},
                                                                      deviceWrapper);

                containerCollection.GetContainer<VertexShader>().Add(vertexShader, material.VertexShaderId);
            }

            if (!containerCollection.GetContainer<PixelShader>().Contains(material.PixelShaderId))
            {
                var pixelShader = PixelShaderFactory.CreateFromFile(material.PixelShaderId, deviceWrapper);
                containerCollection.GetContainer<PixelShader>().Add(pixelShader, material.PixelShaderId);
            }

            foreach (var textureSamplerPair in material.TextureSamplerPairs)
            {
                if (!containerCollection.GetContainer<Texture2D>().Contains(textureSamplerPair.Texture))
                {
                    var texture = TextureFactory.CreateFromFile(textureSamplerPair.Texture, deviceWrapper);
                    containerCollection.GetContainer<Texture2D>().Add(texture, textureSamplerPair.Texture);
                }

                if (!containerCollection.GetContainer<SamplerState>().Contains(textureSamplerPair.SamplerState))
                {
                    var samplerState = SamplerStateFactory.CreateFromFile(textureSamplerPair.SamplerState, deviceWrapper);
                    containerCollection.GetContainer<SamplerState>().Add(samplerState, textureSamplerPair.SamplerState);
                }
            }

            if (!containerCollection.GetContainer<D3D11.RasterizerState>().Contains(material.RasterizerStateId))
            {
                var rasterizerState = RasterizerStateFactory.CreateFromFile(material.RasterizerStateId, deviceWrapper);
                containerCollection.GetContainer<D3D11.RasterizerState>().Add(rasterizerState, material.RasterizerStateId);
            }

            if (!containerCollection.GetContainer<D3D11.DepthStencilState>().Contains(material.DepthStencilStateId))
            {
                var depthStencilState = DepthStencilStateFactory.CreateFromFile(material.DepthStencilStateId, deviceWrapper);
                containerCollection.GetContainer<D3D11.DepthStencilState>().Add(depthStencilState, material.DepthStencilStateId);
            }
        }
    }
}
