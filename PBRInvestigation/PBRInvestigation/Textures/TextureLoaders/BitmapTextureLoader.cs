﻿using System;
using SharpDX;
using SharpDX.WIC;
using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Device;

namespace PBRInvestigation.Textures.TextureLoaders
{
    public class BitmapTextureLoader
    {
        public static D3D11.Resource CreateTexture2DFromBitmap(DeviceWrapper deviceWrapper, TextureInfo info)
        {
            var bitmap = LoadBitmap(new SharpDX.WIC.ImagingFactory2(), info.SourcePath);

            //Assuming four bytes per pixel
            var stride = bitmap.Size.Width * 4;

            using (var buffer = new DataStream(bitmap.Size.Height * stride, true, true))
            {
                bitmap.CopyPixels(stride, buffer);

                var rawTexture = new D3D11.Texture2D(deviceWrapper.Device, new D3D11.Texture2DDescription
                {
                    Width = bitmap.Size.Width,
                    Height = bitmap.Size.Height,
                    ArraySize = info.ArraySize,
                    BindFlags = info.GetBindFlagsEnum(),
                    Usage = info.Usage,
                    CpuAccessFlags = D3D11.CpuAccessFlags.None,
                    Format = info.Format,
                    MipLevels = info.MipLevels,
                    OptionFlags = info.ResourceOption,
                    SampleDescription = info.SampleDescripiton
                },
                new DataRectangle(buffer.DataPointer, stride));

                return rawTexture;
            }
        }

        private static BitmapSource LoadBitmap(ImagingFactory2 factory, String fileName)
        {
            var decoder = new BitmapDecoder(factory,
                                            fileName,
                                            DecodeOptions.CacheOnDemand);

            var formatConverter = new FormatConverter(factory);

            formatConverter.Initialize(decoder.GetFrame(0),
                                       PixelFormat.Format32bppRGBA,
                                       BitmapDitherType.None,
                                       null,
                                       0.0,
                                       BitmapPaletteType.Custom);

            return formatConverter;
        }
    }
}
