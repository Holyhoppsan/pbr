﻿using System;
using System.Collections.Generic;

using D3D11 = SharpDX.Direct3D11;
using PBRInvestigation.Textures.TextureLoaders;
using PBRInvestigation.Textures.ResourceViews;
using PBRInvestigation.Device;
using PBRInvestigation.Serialization;

namespace PBRInvestigation.Textures
{
    public class TextureFactory
    {
        public static Texture2D CreateFromFile(string path, DeviceWrapper deviceWrapper)
        {
            var info = JsonSerializer.Deserialize<TextureInfo>(path);

            try
            {
                if (info.SourcePath != null)
                {
                    var ext = System.IO.Path.GetExtension(info.SourcePath);

                    if (ext.ToLower() == ".dds")
                    {
                        bool isCube;
                        var texture = DDSTextureLoader.CreateTextureFromDDS(deviceWrapper, System.IO.File.ReadAllBytes(info.SourcePath), out isCube) as D3D11.Texture2D;
                        ValidateTextureInfo(info, texture.Description);

                        return Create(info, texture, deviceWrapper);
                    }
                    else
                    { 
                        var texture = BitmapTextureLoader.CreateTexture2DFromBitmap(deviceWrapper, info) as D3D11.Texture2D;
                        return Create(info, texture, deviceWrapper);
                    }
                }
                else
                {
                    throw new ArgumentException("The source path defined in the texture info is not valid");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occured when loading texture: " + e.Message);
                return null;
            }
        }

        public static Texture2D Create(TextureInfo info, DeviceWrapper deviceWrapper)
        {
            try
            {
                var rawTexture = new D3D11.Texture2D(deviceWrapper.Device, CreateTextureDescription(info));
                return Create(info, rawTexture, deviceWrapper);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occured when creating texture: " + e.Message);
                return null;
            }       
        }

        public static Texture2D Create(TextureInfo info, D3D11.Texture2D resource, DeviceWrapper deviceWrapper)
        {
            try
            {
                ValidateTextureInfo(info, resource.Description);

                var srv = CreateShaderResourceView(info, resource, deviceWrapper);

                var dsv = CreateDepthStencilView(info, resource, deviceWrapper);

                var renderTargetViews = new List<D3D11.RenderTargetView>();
                for(var mipIndex = 0; mipIndex < info.MipLevels; mipIndex++)
                {
                    var rtv = CreateRenderTargetView(info, resource, mipIndex, deviceWrapper);
                    renderTargetViews.Add(rtv);
                }

                return new Texture2D(info, resource, srv, dsv, renderTargetViews);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Exception occured when creating texture: " + e.Message);
                return null;
            }
        }

        private static D3D11.ShaderResourceView CreateShaderResourceView(TextureInfo info, D3D11.Resource resource, DeviceWrapper deviceWrapper)
        {
            D3D11.ShaderResourceView srv = null;
            if (info.BindFlags.Contains(D3D11.BindFlags.ShaderResource))
            {
                if (info.ShaderResourceViewInfo != null)
                {
                    srv = ShaderResourceViewFactory.CreateShaderResourceView(deviceWrapper, resource, info.ShaderResourceViewInfo, info.MipLevels);
                }
            }

            return srv;
        }

        private static D3D11.DepthStencilView CreateDepthStencilView(TextureInfo info, D3D11.Resource resource, DeviceWrapper deviceWrapper)
        {
            D3D11.DepthStencilView dsv = null;
            if (info.BindFlags.Contains(D3D11.BindFlags.DepthStencil))
            {
                if (info.DepthStencilViewInfo != null)
                {
                    dsv = DepthStencilViewFactory.CreateDepthStencilView(deviceWrapper, resource, info.DepthStencilViewInfo);
                }
            }

            return dsv;
        }

        private static D3D11.RenderTargetView CreateRenderTargetView(TextureInfo info, D3D11.Resource resource, int mipSlice, DeviceWrapper deviceWrapper)
        {
            D3D11.RenderTargetView rtv = null;
            if (info.BindFlags.Contains(D3D11.BindFlags.RenderTarget))
            {
                if (info.RenderTargetViewInfo != null)
                {
                    rtv = RenderTargetViewFactory.CreateRenderTargetView(deviceWrapper, resource, mipSlice, info.RenderTargetViewInfo);
                }
            }

            return rtv;
        }

        private static D3D11.Texture2DDescription CreateTextureDescription(TextureInfo info)
        {
            return new D3D11.Texture2DDescription()
            {
                ArraySize = info.ArraySize,
                BindFlags = info.GetBindFlagsEnum(),
                Format = info.Format,
                Height = info.Height,
                Width = info.Width,
                CpuAccessFlags = info.CpuAccsessFlags,
                MipLevels = info.MipLevels,
                OptionFlags = info.ResourceOption,
                SampleDescription = info.SampleDescripiton,
                Usage = info.Usage
            };
        }

        private static void ValidateTextureInfo(TextureInfo info, D3D11.Texture2DDescription desc)
        {
            if(info.ArraySize != desc.ArraySize)
            {
                throw new ArgumentException("The value ArraySize does not match between info and description for texture 2D");
            }

            if (info.GetBindFlagsEnum() != desc.BindFlags)
            {
                throw new ArgumentException("The value BindFlags does not match between info and description for texture 2D");
            }

            if (info.Usage != desc.Usage)
            {
                throw new ArgumentException("The value Usage does not match between info and description for texture 2D");
            }

            if (info.Format != desc.Format)
            {
                throw new ArgumentException("The value format does not match between info and description for texture 2D");
            }

            if (info.Width != desc.Width)
            {
                throw new ArgumentException("The value Width does not match between info and description for texture 2D");
            }

            if (info.Height != desc.Height)
            {
                throw new ArgumentException("The value Height does not match between info and description for texture 2D");
            }

            if (info.ResourceOption != desc.OptionFlags)
            {
                throw new ArgumentException("The value OptionFlags does not match between info and description for texture 2D");
            }

            if (info.SampleDescripiton.Count != desc.SampleDescription.Count || 
                info.SampleDescripiton.Quality != desc.SampleDescription.Quality)
            {
                throw new ArgumentException("The value SampleDescripiton does not match between info and description for texture 2D");
            }

            if (info.MipLevels != desc.MipLevels)
            {
                throw new ArgumentException("The value MipLevels does not match between info and description for texture 2D");
            }

            if (info.CpuAccsessFlags != desc.CpuAccessFlags)
            {
                throw new ArgumentException("The value CpuAccessFlags does not match between info and description for texture 2D");
            }
        }
    }
}
