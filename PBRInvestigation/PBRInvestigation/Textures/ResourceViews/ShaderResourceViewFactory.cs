﻿using SharpDX.Direct3D11;
using SharpDX.Direct3D;

using PBRInvestigation.Device;

namespace PBRInvestigation.Textures.ResourceViews
{
    public class ShaderResourceViewFactory
    {
        public static ShaderResourceView CreateShaderResourceView(DeviceWrapper deviceWrapper, Resource resource, ShaderResourceViewInfo info, int mipLevels)
        {
            ShaderResourceView srv = null;

            if (info != null)
            {
                var shaderResourceViewDesc = new ShaderResourceViewDescription();
                shaderResourceViewDesc.Format = info.Format;

                switch (info.ViewDimension)
                {
                    case ShaderResourceViewDimension.Texture2D:
                        {
                            shaderResourceViewDesc.Dimension = ShaderResourceViewDimension.Texture2D;
                            shaderResourceViewDesc.Texture2D.MipLevels = mipLevels;
                            shaderResourceViewDesc.Texture2D.MostDetailedMip = info.MostDetailedMip;
                        }
                        break;
                    case ShaderResourceViewDimension.TextureCube:
                        {
                            shaderResourceViewDesc.Dimension = ShaderResourceViewDimension.TextureCube;
                            shaderResourceViewDesc.TextureCube.MipLevels = mipLevels;
                            shaderResourceViewDesc.TextureCube.MostDetailedMip = info.MostDetailedMip;
                        }
                        break;
                }

                srv = new ShaderResourceView(deviceWrapper.Device, resource, shaderResourceViewDesc);
            }

            return srv;
        }
    }
}
