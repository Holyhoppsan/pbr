﻿using SharpDX.Direct3D11;

using PBRInvestigation.Device;

namespace PBRInvestigation.Textures.ResourceViews
{
    public class DepthStencilViewFactory
    {
        public static DepthStencilView CreateDepthStencilView(DeviceWrapper deviceWrapper, Resource resource, DepthStencilViewInfo info)
        {
            DepthStencilView dsv = null;
           
            if (info != null)
            {
                var depthStencilViewDesc = new DepthStencilViewDescription();
                depthStencilViewDesc.Format = info.Format;

                switch (info.ViewDimension)
                {
                    case DepthStencilViewDimension.Texture2D:
                        {
                            depthStencilViewDesc.Dimension = DepthStencilViewDimension.Texture2D;
                        }
                        break;
                }

                dsv = new DepthStencilView(deviceWrapper.Device, resource, depthStencilViewDesc);
            }
            
            return dsv;
        }
    }
}
