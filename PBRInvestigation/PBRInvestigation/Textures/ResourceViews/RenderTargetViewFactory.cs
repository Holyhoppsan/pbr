﻿using SharpDX.Direct3D11;

using PBRInvestigation.Device;

namespace PBRInvestigation.Textures.ResourceViews
{
    public class RenderTargetViewFactory
    {
        public static RenderTargetView CreateRenderTargetView(DeviceWrapper deviceWrapper, Resource resource, int mipSlice, RenderTargetViewInfo info)
        {
            RenderTargetView rtv = null;
            if (info != null)
            {
                var renderTargetViewDesc = new RenderTargetViewDescription();
                renderTargetViewDesc.Format = info.Format;

                switch (info.ViewDimension)
                {
                    case RenderTargetViewDimension.Texture2D:
                        {
                            renderTargetViewDesc.Dimension = RenderTargetViewDimension.Texture2D;
                            renderTargetViewDesc.Texture2D.MipSlice = mipSlice;
                        }
                        break;
                }

                rtv = new RenderTargetView(deviceWrapper.Device, resource, renderTargetViewDesc);
            }

            return rtv;

        }
    }
}
