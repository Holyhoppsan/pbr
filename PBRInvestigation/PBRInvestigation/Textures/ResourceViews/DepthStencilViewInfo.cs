﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace PBRInvestigation.Textures.ResourceViews
{
    public class DepthStencilViewInfo
    {
        public DepthStencilViewInfo()
        {

        }

        public DepthStencilViewInfo(DepthStencilViewDimension viewDimension,
                                    DepthStencilViewFlags viewFlags,
                                    Format format)
        {
            ViewDimension = viewDimension;
            ViewFlags = viewFlags;
            Format = format;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public DepthStencilViewDimension ViewDimension { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public DepthStencilViewFlags ViewFlags { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Format Format { get; set; }
    }
}
