﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using SharpDX.Direct3D;
using SharpDX.DXGI;

namespace PBRInvestigation.Textures.ResourceViews
{
    public class ShaderResourceViewInfo
    {
        public ShaderResourceViewInfo()
        {

        }

        public ShaderResourceViewInfo(ShaderResourceViewDimension viewDimension, 
                                      int mostDetailedMip,
                                      Format format)
        {
            ViewDimension = viewDimension;
            MostDetailedMip = mostDetailedMip;
            Format = format;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public ShaderResourceViewDimension ViewDimension { get; set; }

        public int MostDetailedMip { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Format Format { get; set; }
    }
}
