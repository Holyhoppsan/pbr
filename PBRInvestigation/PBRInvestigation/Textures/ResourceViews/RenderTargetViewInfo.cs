﻿using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using SharpDX.Direct3D11;
using SharpDX.DXGI;

namespace PBRInvestigation.Textures.ResourceViews
{
    public class RenderTargetViewInfo
    {
        public RenderTargetViewInfo()
        {

        }

        public RenderTargetViewInfo(RenderTargetViewDimension viewDimension,
                                    DepthStencilViewFlags viewFlags,
                                    Format format)
        {
            ViewDimension = viewDimension;
            Format = format;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public RenderTargetViewDimension ViewDimension { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public Format Format { get; set; }

    }
}
