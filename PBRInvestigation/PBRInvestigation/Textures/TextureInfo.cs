﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using D3D11 = SharpDX.Direct3D11;
using DXGI = SharpDX.DXGI;
using System.Collections.Generic;
using PBRInvestigation.Textures.ResourceViews;

namespace PBRInvestigation.Textures
{
    public class TextureInfo
    {
        public TextureInfo()
        {

        }

        public TextureInfo(string sourcePath,
                           int arraySize,
                           List<D3D11.BindFlags> bindFlags,
                           DXGI.Format format,
                           D3D11.ResourceOptionFlags resourceOptionFlags,
                           D3D11.ResourceUsage usage,
                           D3D11.CpuAccessFlags cpuAccessFlags,
                           int mipLevels,
                           int width,
                           int height,
                           DXGI.SampleDescription sampleDescription,
                           ShaderResourceViewInfo shaderResourceViewInfo,
                           DepthStencilViewInfo depthStencilViewInfo,
                           RenderTargetViewInfo renderTargetViewInfo)
        {
            SourcePath = sourcePath;
            ArraySize = arraySize;
            BindFlags = bindFlags;
            Format = format;
            ResourceOption = resourceOptionFlags;
            Usage = usage;
            CpuAccsessFlags = cpuAccessFlags;
            MipLevels = mipLevels;
            Width = width;
            Height = height;
            SampleDescripiton = sampleDescription;
            ShaderResourceViewInfo = shaderResourceViewInfo;
            DepthStencilViewInfo = depthStencilViewInfo;
            RenderTargetViewInfo = renderTargetViewInfo;
        }

        public string SourcePath { get; set; }

        public int ArraySize { get; set; }

        [JsonProperty("BindFlags", ItemConverterType =typeof(StringEnumConverter))]
        public List<D3D11.BindFlags> BindFlags { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public DXGI.Format Format { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.ResourceOptionFlags ResourceOption { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.ResourceUsage Usage;

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.CpuAccessFlags CpuAccsessFlags;

        public int MipLevels { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public DXGI.SampleDescription SampleDescripiton { get; set; }

        public ShaderResourceViewInfo ShaderResourceViewInfo { get; set; }

        public DepthStencilViewInfo DepthStencilViewInfo { get; set; }

        public RenderTargetViewInfo RenderTargetViewInfo { get; set; }

        public D3D11.BindFlags GetBindFlagsEnum()
        {
            D3D11.BindFlags bindFlags = D3D11.BindFlags.None;

            foreach (var entry in BindFlags)
            {
                bindFlags |= entry;
            }

            return bindFlags;
        }
    }
}
