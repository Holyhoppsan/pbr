﻿using System;
using System.Collections.Generic;

using SharpDX;
using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.Textures
{
    public class Texture2D : IDisposable
    {
        public Texture2D(TextureInfo info, 
                         D3D11.Resource data, 
                         D3D11.ShaderResourceView srv, 
                         D3D11.DepthStencilView dsv,
                         List<D3D11.RenderTargetView> rtvs)
        {
            Info = info;
            Data = data;

            ShaderResourceView = srv;
            DepthStencilView = dsv;
            RenderTargetViews = rtvs;
        }

        public TextureInfo Info { get; private set; }
        public D3D11.Resource Data { get; private set; }

        public D3D11.ShaderResourceView ShaderResourceView { get; private set; }

        public D3D11.DepthStencilView DepthStencilView { get; private set; }

        public List<D3D11.RenderTargetView> RenderTargetViews { get; private set; }

        public void MapStreamToSubresouce(D3D11.DeviceContext context, int subResource, D3D11.MapMode mapMode, D3D11.MapFlags mapFlags, Action<DataStream> updateProcedure)
        {
            DataStream mappedStream;
            context.MapSubresource(Data, subResource, mapMode, mapFlags, out mappedStream);

            updateProcedure(mappedStream);

            context.UnmapSubresource(Data, subResource);

            mappedStream.Dispose();
        }

        public void Dispose()
        {
            Data.Dispose();

            if(ShaderResourceView != null)
            {
                ShaderResourceView.Dispose();
            }

            if(DepthStencilView != null)
            {
                DepthStencilView.Dispose();
            }
            
            foreach(var rtv in RenderTargetViews)
            {
                if (rtv != null)
                {
                    rtv.Dispose();
                }
            } 
        }
    }
}
