﻿using System;
using System.Collections.Generic;

using PBRInvestigation.Pipeline.Pass;
using PBRInvestigation.Textures;
using PBRInvestigation.Device;

using PBRInvestigation.Container;

using SharpDX.DXGI;

namespace PBRInvestigation.Pipeline
{
    public class RenderPipeline : IDisposable
    {
        public RenderPipeline(PipelineInfo pipelineInfo, IntPtr handle)
        {
            AssetContainers = new ContainerCollection();

            DeviceWrapper = new DeviceWrapper(pipelineInfo.DeviceInfo, AssetContainers, handle);         

            CreatePipelineTextures(pipelineInfo.Textures);

            mPasses = new SortedList<int, IPass>();
        }

        public void RenderPasses()
        {
            foreach(var pass in mPasses)
            {
                pass.Value.Render();
            }

           // RenderDebug();

            DeviceWrapper.SwapChain.Present(1, PresentFlags.None);
        }

        public void AddPass(int index, IPass pass)
        {
            mPasses.Add(index, pass);
        }

        public void RemovePass(int index)
        {
            mPasses.Remove(index);
        }

        public SortedList<int, IPass> Passes
        {
            get
            {
                return mPasses;
            }
        }

        public void Dispose()
        {
            AssetContainers.Dispose();

            DeviceWrapper.Dispose();
        }

        public DeviceWrapper DeviceWrapper { get; private set; }

        public ContainerCollection AssetContainers { get; set; }

        private void CreatePipelineTextures(List<TextureInfo> textures)
        {
            foreach (var texture in textures)
            {
                var createdTexture = TextureFactory.Create(texture, DeviceWrapper);
                AssetContainers.GetContainer<Texture2D>().Add(createdTexture, texture.SourcePath);
            }
        }

        private void RenderDebug()
        {
            foreach (var pass in mPasses)
            {
                pass.Value.RenderDebugInfo();
            }
        }

        private SortedList<int, IPass> mPasses;

    }
}
