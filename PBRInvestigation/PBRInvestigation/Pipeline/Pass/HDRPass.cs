﻿using System;

using SharpDX;
using System.Collections.Generic;

using PBRInvestigation.Device;
using PBRInvestigation.Textures;
using PBRInvestigation.Meshes;
using PBRInvestigation.Container;
using PBRInvestigation.Materials;
using PBRInvestigation.Materials.Constants;

namespace PBRInvestigation.Pipeline.Pass
{
    public class HDRPass : IPass
    {
        public HDRPass(SharpDX.Rectangle viewRectangle, string renderTarget, string depthStencil, DeviceWrapper deviceWrapper, ContainerCollection containerCollection)
        {
            mViewport = new Viewport(viewRectangle.X, viewRectangle.Y, viewRectangle.Width, viewRectangle.Height);

            RenderTarget = renderTarget;

            DepthStencil = depthStencil;

            mDeviceWrapper = deviceWrapper;

            mContainerCollection = containerCollection;

            mLuminanceQuad = new ScreenspaceQuad("LuminanceQuad", new Rectangle(0, 0, 1024, 1024), new Rectangle(0, 0, 1024, 1024), "Assets/Materials/PostProcess/luminance.mat", deviceWrapper, containerCollection);

            mTonemapQuad = new ScreenspaceQuad("TonemapQuad", new Rectangle(0, 0, 1280, 720), new Rectangle(0, 0, 1280, 720), "Assets/Materials/PostProcess/tonemapping.mat", deviceWrapper, containerCollection);

            SetupDebugQuads();
        }

        public string DepthStencil { get; set; }

        public string RenderTarget { get; set; }

        public void Render()
        {
            RenderLuminance();

            RenderTonemapping();
        }

        public void RenderDebugInfo()
        {
            var deviceContext = mDeviceWrapper.ImmediateContext;

            Texture2D backbuffer = null;
            if (mContainerCollection.GetContainer<Texture2D>().TryGetValue("BackBuffer", out backbuffer))
            {
                deviceContext.Rasterizer.SetViewport(new Viewport(new Rectangle(0,0,1280, 720)));

                deviceContext.OutputMerger.SetRenderTargets(null, backbuffer.RenderTargetViews[0]);

                mLuminanceDebugQuad.Render();
            }
        }

        public void ToggleDebugInfo()
        {
            throw new NotImplementedException();
        }

        public Viewport Viewport { get { return mViewport; } }

        private void RenderLuminance()
        {
            var deviceContext = mDeviceWrapper.ImmediateContext;

            Texture2D luminance = null;
            if (mContainerCollection.GetContainer<Texture2D>().TryGetValue("Luminance", out luminance))
            {
                deviceContext.Rasterizer.SetViewport(Viewport);

                deviceContext.ClearRenderTargetView(luminance.RenderTargetViews[0], new Color(128, 255, 128));

                deviceContext.OutputMerger.SetRenderTargets(null, luminance.RenderTargetViews[0]);

                mLuminanceQuad.Render();

                deviceContext.GenerateMips(luminance.ShaderResourceView);    
            }
        }

        private void RenderTonemapping()
        {
            var deviceContext = mDeviceWrapper.ImmediateContext;

            Texture2D backbuffer = null;
            if (mContainerCollection.GetContainer<Texture2D>().TryGetValue(RenderTarget, out backbuffer))
            {
                deviceContext.Rasterizer.SetViewport(new Viewport(new Rectangle(0, 0, 1280, 720)));

                deviceContext.OutputMerger.SetRenderTargets(null, backbuffer.RenderTargetViews[0]);

                mTonemapQuad.Render();
            }
        }

        private void SetupDebugQuads()
        {
            var luminanceDebugMaterialHeader = new MaterialHeader("Assets/Shaders/PostProcess/screenspacequadvertex.hlsl",
                                                               "Assets/Shaders/PostProcess/resolvepixel.hlsl",
                                                               new List<TextureSamplerPair> { new TextureSamplerPair { SamplerState = "Assets/SamplerStates/wrapminmagmiplinearwithlod.sampler", Texture = "Luminance" } },
                                                               "Assets/RasterizerStates/default.raster",
                                                               "Assets/DepthStencilStates/depthdisabled.depsten");
            var luminanceDebugConstants = new ResolveConstants();
            var material = MaterialFactory.Create("LuminanceDebugMaterial", luminanceDebugMaterialHeader, luminanceDebugConstants, mContainerCollection, mDeviceWrapper);

            mContainerCollection.GetContainer<IMaterial>().Add(material, "LuminanceDebugMaterial");

            mLuminanceDebugQuad = new ScreenspaceQuad("LuminanceDebugQuad", new Rectangle(0, 0, 256, 256), new Rectangle(0, 0, 1280, 720), "LuminanceDebugMaterial", mDeviceWrapper, mContainerCollection);
        }

        private DeviceWrapper mDeviceWrapper;

        private ContainerCollection mContainerCollection;

        private ScreenspaceQuad mLuminanceQuad;

        private ScreenspaceQuad mTonemapQuad;

        private ScreenspaceQuad mLuminanceDebugQuad;

        private Viewport mViewport;
    }
}
