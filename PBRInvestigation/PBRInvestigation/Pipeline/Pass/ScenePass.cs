﻿using System;

using SharpDX;
using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Device;
using PBRInvestigation.Textures;
using PBRInvestigation.Scenes;
using PBRInvestigation.Buffers.ConstantBuffers;
using PBRInvestigation.Buffers.VertexBuffers;
using PBRInvestigation.Buffers.IndexBuffers;
using PBRInvestigation.Materials;
using PBRInvestigation.Shaders;
using PBRInvestigation.SamplerStates;
using PBRInvestigation.Meshes;

using PBRInvestigation.Container;

namespace PBRInvestigation.Pipeline.Pass
{
    public class ScenePass : IPass
    {
        public ScenePass(SharpDX.Rectangle viewRectangle, DeviceWrapper deviceWrapper, ContainerCollection containerCollection)
        {
            mViewport = new Viewport(viewRectangle.X, viewRectangle.Y, viewRectangle.Width, viewRectangle.Height);
            mDeviceWrapper = deviceWrapper;
            mContainerCollection = containerCollection;
        }

        public void SetScene(Scene scene)
        {
            mScene = scene;
        } 

        public void SetView(View view)
        {
            mView = view;
        }

        public void Render()
        {
            var deviceContext = mDeviceWrapper.ImmediateContext;

            Texture2D backBuffer = null;
            if (mContainerCollection.GetContainer<Texture2D>().TryGetValue(RenderTarget, out backBuffer))
            {
                deviceContext.ClearRenderTargetView(backBuffer.RenderTargetViews[0], new SharpDX.Color(128, 128, 128));

                Texture2D depthBuffer = null;
                if (mContainerCollection.GetContainer<Texture2D>().TryGetValue(DepthStencil, out depthBuffer))
                {
                    deviceContext.ClearDepthStencilView(depthBuffer.DepthStencilView, D3D11.DepthStencilClearFlags.Depth, 1.0f, 0);

                    deviceContext.OutputMerger.SetRenderTargets(depthBuffer.DepthStencilView, backBuffer.RenderTargetViews[0]);

                    IConstantBuffer viewBuffer = null;
                    mContainerCollection.GetContainer<IConstantBuffer>().TryGetValue("MainViewviewbuffer", out viewBuffer);

                    IConstantBuffer lightBuffer = null;
                    mContainerCollection.GetContainer<IConstantBuffer>().TryGetValue(mScene.LightSettings.Identifier, out lightBuffer);

                    deviceContext.Rasterizer.SetViewport(Viewport);

                    foreach (var meshLookup in mScene.Meshes)
                    {
                        var mesh = meshLookup.Value;
                        mesh.UpdateBuffer(deviceContext);

                        IConstantBuffer meshBuffer = null;
                        mContainerCollection.GetContainer<IConstantBuffer>().TryGetValue(mesh.Buffer, out meshBuffer);

                        foreach (var subModel in mesh.ModelData.SubModelsIDs)
                        {
                            IMaterial material = null;
                            if (mContainerCollection.GetContainer<IMaterial>().TryGetValue(mesh.Material, out material))
                            {
                                IVertexBuffer vertexBuffer = null;
                                if (mContainerCollection.GetContainer<IVertexBuffer>().TryGetValue(subModel.Item1, out vertexBuffer))
                                {
                                    deviceContext.InputAssembler.SetVertexBuffers(0, vertexBuffer.Binding);
                                }

                                IIndexBuffer indexBuffer = null;
                                if (subModel.Item2 != Mesh.NoIndexBuffer)
                                {
                                    if (mContainerCollection.GetContainer<IIndexBuffer>().TryGetValue(subModel.Item2, out indexBuffer))
                                    {
                                        deviceContext.InputAssembler.SetIndexBuffer(indexBuffer.Buffer, indexBuffer.Format, 0);
                                    }
                                }

                                deviceContext.InputAssembler.PrimitiveTopology = SharpDX.Direct3D.PrimitiveTopology.TriangleList;

                                IConstantBuffer materialBuffer = null;
                                mContainerCollection.GetContainer<IConstantBuffer>().TryGetValue(material.BufferId, out materialBuffer);

                                //Set vertex shader
                                VertexShader vertexShader = null;
                                if (mContainerCollection.GetContainer<VertexShader>().TryGetValue(material.VertexShaderId, out vertexShader))
                                {
                                    deviceContext.VertexShader.Set(vertexShader.Shader);
                                    deviceContext.InputAssembler.InputLayout = vertexShader.InputLayout;
                                }

                                //Set view constant buffer (vertex shader)
                                deviceContext.VertexShader.SetConstantBuffer(0, viewBuffer.Buffer);
                                //Set mesh specific constant buffer (vertex shader)
                                deviceContext.VertexShader.SetConstantBuffer(1, meshBuffer.Buffer);
                                //Set light constant buffer (vertex shader)
                                deviceContext.VertexShader.SetConstantBuffer(2, lightBuffer.Buffer);
                                //set the material constant buffer
                                deviceContext.VertexShader.SetConstantBuffer(3, materialBuffer.Buffer);

                                //Set pixel shader
                                PixelShader pixelShader = null;
                                if (mContainerCollection.GetContainer<PixelShader>().TryGetValue(material.PixelShaderId, out pixelShader))
                                {
                                    deviceContext.PixelShader.Set(pixelShader.Shader);
                                }

                                //Set view constant buffer (pixel shader shader)
                                deviceContext.PixelShader.SetConstantBuffer(0, viewBuffer.Buffer);
                                //Set mesh specific constant buffer (vertex shader)
                                deviceContext.PixelShader.SetConstantBuffer(1, meshBuffer.Buffer);
                                //Set light constant buffer (vertex shader)
                                deviceContext.PixelShader.SetConstantBuffer(2, lightBuffer.Buffer);
                                //set the material constant buffer
                                deviceContext.PixelShader.SetConstantBuffer(3, materialBuffer.Buffer);

                                for (int slotIndex = 0; slotIndex < material.TextureSamplerPairs.Count; slotIndex++)
                                {
                                    var textureSamplerPair = material.TextureSamplerPairs[slotIndex];

                                    Texture2D texture = null;
                                    if (mContainerCollection.GetContainer<Texture2D>().TryGetValue(textureSamplerPair.Texture, out texture))
                                    {
                                        deviceContext.PixelShader.SetShaderResource(slotIndex, texture.ShaderResourceView);
                                    }

                                    SamplerState samplerState = null;
                                    if (mContainerCollection.GetContainer<SamplerState>().TryGetValue(textureSamplerPair.SamplerState, out samplerState))
                                    {
                                        deviceContext.PixelShader.SetSampler(slotIndex, samplerState.Data);
                                    }
                                }

                                D3D11.RasterizerState rasterizerState = null;
                                if (mContainerCollection.GetContainer<D3D11.RasterizerState>().TryGetValue(material.RasterizerStateId, out rasterizerState))
                                {
                                    deviceContext.Rasterizer.State = rasterizerState;
                                }

                                D3D11.DepthStencilState depthStencilState = null;
                                if (mContainerCollection.GetContainer<D3D11.DepthStencilState>().TryGetValue(material.DepthStencilStateId, out depthStencilState))
                                {
                                    deviceContext.OutputMerger.SetDepthStencilState(depthStencilState);
                                }

                                if (indexBuffer != null)
                                {
                                    deviceContext.DrawIndexed(indexBuffer.Count, 0, 0);
                                }
                                else
                                {
                                    deviceContext.Draw(vertexBuffer.Count, 0);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void RenderDebugInfo()
        {
        }

        public void ToggleDebugInfo()
        {
            throw new NotImplementedException();
        }

        public Viewport Viewport { get { return mViewport; } }

        public string DepthStencil { get; set; }

        public string RenderTarget { get; set; }

        private DeviceWrapper mDeviceWrapper;

        private ContainerCollection mContainerCollection;

        private Scene mScene;

        private View mView;

        private Viewport mViewport;
    }
}
