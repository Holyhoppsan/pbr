﻿using SharpDX;

namespace PBRInvestigation.Pipeline.Pass
{
    public interface IPass
    {
        void Render();
        void RenderDebugInfo();
        void ToggleDebugInfo();

        Viewport Viewport { get; }

        string RenderTarget { get; }

        string DepthStencil { get;}
    }
}
