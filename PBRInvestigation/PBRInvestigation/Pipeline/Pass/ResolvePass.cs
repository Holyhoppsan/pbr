﻿using System;

using SharpDX;

using PBRInvestigation.Device;
using PBRInvestigation.Textures;
using PBRInvestigation.Meshes;
using PBRInvestigation.Container;

namespace PBRInvestigation.Pipeline.Pass
{
    public class ResolvePass : IPass
    {
        public ResolvePass(SharpDX.Rectangle viewRectangle, DeviceWrapper deviceWrapper, ContainerCollection containerCollection)
        {
            mViewport = new Viewport(viewRectangle.X, viewRectangle.Y, viewRectangle.Width, viewRectangle.Height);

            mDeviceWrapper = deviceWrapper;

            mContainerCollection = containerCollection;

            mResolveQuad = new ScreenspaceQuad("ResolveQuad", new Rectangle(0, 0, 1280, 720), new Rectangle(0,0,1280, 720), "Assets/Materials/resolve.mat", deviceWrapper, containerCollection);
        }

        public string DepthStencil { get; set; }

        public string RenderTarget { get; set; }

        public void Render()
        {
            var deviceContext = mDeviceWrapper.ImmediateContext;

            Texture2D backBuffer = null;
            if (mContainerCollection.GetContainer<Texture2D>().TryGetValue(RenderTarget, out backBuffer))
            {
                deviceContext.Rasterizer.SetViewport(Viewport);

                deviceContext.ClearRenderTargetView(backBuffer.RenderTargetViews[0], new Color(128, 255, 128));

                deviceContext.OutputMerger.SetRenderTargets(null, backBuffer.RenderTargetViews[0]);

                mResolveQuad.Render();
            }
        }

        public void RenderDebugInfo()
        {
        }

        public void ToggleDebugInfo()
        {
            throw new NotImplementedException();
        }

        public Viewport Viewport { get { return mViewport; } }

        private DeviceWrapper mDeviceWrapper;

        private ContainerCollection mContainerCollection;

        private ScreenspaceQuad mResolveQuad;

        private Viewport mViewport;
    }
}
