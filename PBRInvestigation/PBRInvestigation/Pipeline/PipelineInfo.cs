﻿using System.Collections.Generic;

using PBRInvestigation.Textures;
using PBRInvestigation.Device;

namespace PBRInvestigation.Pipeline
{
    public class PipelineInfo
    {
        public PipelineInfo()
        {
            DeviceInfo = new DeviceInfo();
            Textures = new List<TextureInfo>();
        }

        public DeviceInfo DeviceInfo { get; set; }
        public List<TextureInfo> Textures { get; set; }
    }
}
