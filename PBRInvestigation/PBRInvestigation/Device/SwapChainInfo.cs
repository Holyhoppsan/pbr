﻿using System.Collections.Generic;

using SharpDX.Direct3D;
using D3D11 = SharpDX.Direct3D11;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PBRInvestigation.Device
{
    public class SwapChainInfo
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public DriverType DriverType { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.DeviceCreationFlags DeviceCreationFlags { get; set; }

        public BackBufferInfo BackBufferInfo { get; set; }

        public int BackBufferCount { get; set; }
        
        public bool IsWindowed { get; set; }
    }
}
