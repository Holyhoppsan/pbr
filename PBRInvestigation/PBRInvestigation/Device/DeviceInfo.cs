﻿
namespace PBRInvestigation.Device
{
    public class DeviceInfo
    {
        public DeviceInfo()
        {
            SwapChain = new SwapChainInfo();
        }

        public SwapChainInfo SwapChain { get; set; }
    }
}
