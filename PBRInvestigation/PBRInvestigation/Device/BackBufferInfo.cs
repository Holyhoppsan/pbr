﻿using System.Collections.Generic;

using SharpDX.DXGI;

using PBRInvestigation.Textures;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PBRInvestigation.Device
{
    public class BackBufferInfo
    {
        public BackBufferInfo()
        {
            TextureInfo = new TextureInfo();
        }

        public TextureInfo TextureInfo { get; set; }

        public Rational RefreshRate { get; set; }

        [JsonProperty("Usage", ItemConverterType = typeof(StringEnumConverter))]
        public List<Usage> Usage { get; set; }

        public Usage GetUsageEnum()
        {
            Usage backBufferUsage = SharpDX.DXGI.Usage.BackBuffer;

            foreach (var entry in Usage)
            {
                backBufferUsage |= entry;
            }

            return backBufferUsage;
        }
    }
}
