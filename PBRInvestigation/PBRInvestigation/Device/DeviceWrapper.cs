﻿using System;

using D3D11 = SharpDX.Direct3D11;
using SharpDX.DXGI;

using PBRInvestigation.Textures;
using PBRInvestigation.Container;

namespace PBRInvestigation.Device
{
    public class DeviceWrapper : IDisposable
    {
        public DeviceWrapper(DeviceInfo deviceInfo, ContainerCollection containerCollection, IntPtr handle)
        {
            CreateSwapChain(deviceInfo.SwapChain, containerCollection, handle);

            ImmediateContext = mDevice.ImmediateContext;
        }

        public D3D11.Device Device
        {
            get
            {
                return mDevice;
            }

            private set
            {
                mDevice = value;
            }
        }

        public D3D11.DeviceContext ImmediateContext
        {
            get
            {
                return mImmediateContext;
            }

            private set
            {
                mImmediateContext = value;
            }
        }

        public SwapChain SwapChain
        {
            get
            {
                return mSwapChain;
            }

            private set
            {
                mSwapChain = value;
            }
        }

        public void Dispose()
        {
            mSwapChain.Dispose();
            mImmediateContext.Dispose();
            mDevice.Dispose();
        }

        private void CreateSwapChain(SwapChainInfo swapChainInfo, ContainerCollection containerCollection, IntPtr handle)
        {
            var backBufferInfo = swapChainInfo.BackBufferInfo;

            var backBufferDesc = new ModeDescription(backBufferInfo.TextureInfo.Width, 
                                                     backBufferInfo.TextureInfo.Height, 
                                                     backBufferInfo.RefreshRate, 
                                                     backBufferInfo.TextureInfo.Format);

            var swapChainDesc = new SwapChainDescription()
            {
                ModeDescription = backBufferDesc,
                SampleDescription = backBufferInfo.TextureInfo.SampleDescripiton,
                Usage = backBufferInfo.GetUsageEnum(),
                BufferCount = swapChainInfo.BackBufferCount,
                OutputHandle = handle,
                IsWindowed = swapChainInfo.IsWindowed
            };

            D3D11.Device.CreateWithSwapChain(swapChainInfo.DriverType,
                                             swapChainInfo.DeviceCreationFlags,
                                             swapChainDesc,
                                             out mDevice,
                                             out mSwapChain);

            using (var backBuffer = mSwapChain.GetBackBuffer<D3D11.Texture2D>(0))
            {
                var backBufferTexture = TextureFactory.Create(backBufferInfo.TextureInfo, backBuffer, this);
                containerCollection.GetContainer<Texture2D>().Add(backBufferTexture, backBufferInfo.TextureInfo.SourcePath);
            }
        }

        private D3D11.Device mDevice;
        private D3D11.DeviceContext mImmediateContext;
        private SwapChain mSwapChain;
    }
}
