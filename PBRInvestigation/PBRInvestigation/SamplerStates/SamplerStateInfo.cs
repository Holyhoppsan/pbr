﻿using D3D11 = SharpDX.Direct3D11;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.SamplerStates
{
    public class SamplerStateInfo
    {
        public SamplerStateInfo()
        {

        }

        public SamplerStateInfo(D3D11.TextureAddressMode addressU,
                                D3D11.TextureAddressMode addressV,
                                D3D11.TextureAddressMode addressW,
                                RawColor4 borderColor,
                                D3D11.Comparison comparisonFunction,
                                D3D11.Filter filter,
                                int maximumAnisotropy,
                                float maximumLod,
                                float minimumLod,
                                float mipLodBias)
        {
            AddressU = addressU;
            AddressV = addressV;
            AddressW = addressW;
            BorderColor = borderColor;
            ComparisonFunction = comparisonFunction;
            Filter = filter;
            MaximumAnisotropy = maximumAnisotropy;
            MaximumLod = maximumLod;
            MinimumLod = minimumLod;
            MipLodBias = mipLodBias;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.TextureAddressMode AddressU { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.TextureAddressMode AddressV { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.TextureAddressMode AddressW { get; set; }

        public RawColor4 BorderColor { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.Comparison ComparisonFunction { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.Filter Filter { get; set; }

        public int MaximumAnisotropy { get; set; }

        public float MaximumLod { get; set; }

        public float MinimumLod { get; set; }

        public float MipLodBias { get; set; }
    }
}
