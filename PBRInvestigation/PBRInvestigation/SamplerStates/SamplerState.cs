﻿using System;
using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.SamplerStates
{
    public class SamplerState : IDisposable
    {
        public SamplerState(SamplerStateInfo info, D3D11.SamplerState data)
        {
            Info = info;
            Data = data;
        }

        public SamplerStateInfo Info { get; private set; }

        public D3D11.SamplerState Data { get; private set; }

        public void Dispose()
        {
            Data.Dispose();
        }
    }
}
