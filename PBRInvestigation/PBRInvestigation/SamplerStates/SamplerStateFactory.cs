﻿using PBRInvestigation.Serialization;
using PBRInvestigation.Device;

using D3D11 = SharpDX.Direct3D11;

namespace PBRInvestigation.SamplerStates
{
    public class SamplerStateFactory
    {
        public static SamplerState CreateFromFile(string path, DeviceWrapper deviceWrapper)
        {
            var info = JsonSerializer.Deserialize<SamplerStateInfo>(path);

            return Create(info, path, deviceWrapper);
        }

        public static SamplerState Create(SamplerStateInfo info, string name, DeviceWrapper deviceWrapper)
        {
            try
            {
                var rawSampler = new D3D11.SamplerState(deviceWrapper.Device, CreateSampleStateDescription(info));
                return new SamplerState(info, rawSampler);
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Exception occured when creating sampler state: " + e.Message);
                return null;
            }
        }

        private static D3D11.SamplerStateDescription CreateSampleStateDescription(SamplerStateInfo info)
        {
            var samplerStateDesc = new D3D11.SamplerStateDescription();
            samplerStateDesc.AddressU = info.AddressU;
            samplerStateDesc.AddressV = info.AddressV;
            samplerStateDesc.AddressW = info.AddressW;
            samplerStateDesc.BorderColor = info.BorderColor;
            samplerStateDesc.ComparisonFunction = info.ComparisonFunction;
            samplerStateDesc.Filter = info.Filter;
            samplerStateDesc.MaximumAnisotropy = info.MaximumAnisotropy;
            samplerStateDesc.MaximumLod = info.MaximumLod;
            samplerStateDesc.MinimumLod = info.MinimumLod;
            samplerStateDesc.MipLodBias = info.MipLodBias;

            return samplerStateDesc;
        }
    }
}
