﻿using D3D11 = SharpDX.Direct3D11;

using PBRInvestigation.Device;
using PBRInvestigation.Serialization;

namespace PBRInvestigation.DepthStencilState
{
    public class DepthStencilStateFactory
    {
        public static D3D11.DepthStencilState CreateFromFile(string path, DeviceWrapper deviceWrapper)
        {
            var info = JsonSerializer.Deserialize<DepthStencilStateInfo>(path);

            return Create(info, deviceWrapper);
        }

        public static D3D11.DepthStencilState Create(DepthStencilStateInfo info, DeviceWrapper deviceWrapper)
        {
            var description = new D3D11.DepthStencilStateDescription();

            description.BackFace.Comparison = info.BackFace.Comparison;
            description.BackFace.DepthFailOperation = info.BackFace.DepthFailOperation;
            description.BackFace.FailOperation = info.BackFace.FailOperation;
            description.BackFace.PassOperation = info.BackFace.PassOperation;

            description.DepthComparison = info.DepthComparison;
            description.DepthWriteMask = info.DepthWriteMask;

            description.FrontFace.Comparison = info.FrontFace.Comparison;
            description.FrontFace.DepthFailOperation = info.FrontFace.DepthFailOperation;
            description.FrontFace.FailOperation = info.FrontFace.FailOperation;
            description.FrontFace.PassOperation = info.FrontFace.PassOperation;

            description.IsDepthEnabled = info.IsDepthEnabled;
            description.IsStencilEnabled = info.IsStencilEnabled;
            description.StencilReadMask = info.StencilReadMask;
            description.StencilWriteMask = info.StencilWriteMask;

            return new D3D11.DepthStencilState(deviceWrapper.Device, description);
        }
    }
}
