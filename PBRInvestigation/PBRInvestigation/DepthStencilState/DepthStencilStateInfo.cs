﻿using D3D11 = SharpDX.Direct3D11;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PBRInvestigation.DepthStencilState
{
    public class DepthStencilStateInfo
    {
        public struct DepthStencilOperationDescription
        {
            [JsonConverter(typeof(StringEnumConverter))]
            public D3D11.Comparison Comparison { get; set; }

            [JsonConverter(typeof(StringEnumConverter))]
            public D3D11.StencilOperation DepthFailOperation { get; set; }

            [JsonConverter(typeof(StringEnumConverter))]
            public D3D11.StencilOperation FailOperation { get; set; }

            [JsonConverter(typeof(StringEnumConverter))]
            public D3D11.StencilOperation PassOperation { get; set; }
        }

        public DepthStencilStateInfo()
        {

        }

        public DepthStencilStateInfo(DepthStencilOperationDescription backFace,
                                     D3D11.Comparison depthComparison,
                                     D3D11.DepthWriteMask depthWriteMask,
                                     DepthStencilOperationDescription frontFace,
                                     bool isDepthEnabled,
                                     bool isStencilEnabled,
                                     byte stencilReadMask,
                                     byte stencilWriteMask)
        {
            BackFace = backFace;
            DepthComparison = depthComparison;
            DepthWriteMask = depthWriteMask;
            FrontFace = frontFace;
            IsDepthEnabled = isDepthEnabled;
            IsStencilEnabled = isStencilEnabled;
            StencilReadMask = stencilReadMask;
            StencilWriteMask = stencilWriteMask;
        }

        public DepthStencilOperationDescription BackFace { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.Comparison DepthComparison { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public D3D11.DepthWriteMask DepthWriteMask { get; set; }

        public DepthStencilOperationDescription FrontFace { get; set; }

        public bool IsDepthEnabled { get; set; }

        public bool IsStencilEnabled { get; set; }

        public byte StencilReadMask { get; set; }

        public byte StencilWriteMask { get; set; }
    }
}
