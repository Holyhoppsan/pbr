﻿using SharpDX;
using SharpDX.Mathematics.Interop;

namespace PBRInvestigation.Light
{
    public struct DirectionalLight
    {
        public Vector4 LightDirection { get; set; }
        public RawColor4 LightColor { get; set; }
    }
}
