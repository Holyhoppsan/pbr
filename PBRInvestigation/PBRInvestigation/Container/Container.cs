﻿using System;
using System.Collections.Generic;

namespace PBRInvestigation.Container
{
    public class Container<T> : IContainer where T : IDisposable
    {
        public Container()
        {
            ContentDictionary = new Dictionary<string, T>();
        }

        public void Add(T value, string identifier)
        {
            ContentDictionary.Add(identifier, value);
        }

        public bool Contains(string identifier)
        {
            return ContentDictionary.ContainsKey(identifier);
        }

        public bool TryGetValue(string identifier, out T value)
        {
            return ContentDictionary.TryGetValue(identifier, out value);
        }

        public void Dispose()
        {
            foreach(var pair in ContentDictionary)
            {
                pair.Value.Dispose();
            }
        }

        protected IDictionary<string, T> ContentDictionary { get; set; }
        
    }
}
