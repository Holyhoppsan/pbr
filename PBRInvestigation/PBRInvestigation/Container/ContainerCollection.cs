﻿using System;
using System.Collections.Generic;

namespace PBRInvestigation.Container
{
    public class ContainerCollection : IDisposable
    {
        public ContainerCollection()
        {
            mContainers = new Dictionary<string, IContainer>();
        }

        public Container<T> GetContainer<T>() where T : IDisposable
        {
            var typeName = typeof(T).Name;

            IContainer container = null;
            if(mContainers.TryGetValue(typeName, out container))
            {
                return container as Container<T>;
            }
            else
            {
                var newContainer = new Container<T>();
                mContainers.Add(typeName, newContainer);
                return newContainer;
            }
        }

        public void Dispose()
        {
            foreach(var entry in mContainers)
            {
                entry.Value.Dispose();
            }
        }

        private Dictionary<string, IContainer> mContainers;
    }
}
