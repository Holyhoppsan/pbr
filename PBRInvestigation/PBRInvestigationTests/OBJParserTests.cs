﻿using System;
using NUnit.Framework;
using PBRInvestigation.Models.OBJLoader;

namespace PBRInvestigationTests
{
    [TestFixture]
    public class OBJParserTests
    {
        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataThrowsAnArgumentNullExceptionhenPassedNullAsInput()
        {
            var loader = new OBJParser();

            var testDelegate = new TestDelegate(
                () =>
                {
                    loader.ParseOBJData(null);
                }
            );

            Assert.Throws<ArgumentNullException>(testDelegate, "ParseOBJData did not throw an argument null exception when passed null as input");
        } 

        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataReturnAnEmptyParsedObjectDataWhenPassedEmptyString()
        {
            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData("");

            Assert.That(parsedData.Count == 0);
        }

        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataCanParseAValidVertexLine()
        {
            var inputString = "v -2.000000 -3.000000 -4.000000";

            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData(inputString);

            Assert.That(parsedData[0].Positions.Count == 1);

            var vertex = parsedData[0].Positions[0];
            var epsilon = 0.001f;
            Assert.True((vertex.X - 2.0f) < epsilon);
            Assert.True((vertex.Y - 3.0f) < epsilon);
            Assert.True((vertex.Z - 4.0f) < epsilon);
        }

        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataCanParseInputWithMultpleLines()
        {
            var inputString = "v -2.000000 -3.000000 -4.000000\nv -3.000000 -3.000000 -4.000000";

            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData(inputString);

            var parsedDataItem = parsedData[0];
            Assert.That(parsedDataItem.Positions.Count == 2);

            var vertex = parsedDataItem.Positions[0];
            var epsilon = 0.001f;
            Assert.True((vertex.X - 2.0f) < epsilon);
            Assert.True((vertex.Y - 3.0f) < epsilon);
            Assert.True((vertex.Z - 4.0f) < epsilon);

            vertex = parsedDataItem.Positions[1];
            Assert.True((vertex.X - 3.0f) < epsilon);
            Assert.True((vertex.Y - 3.0f) < epsilon);
            Assert.True((vertex.Z - 4.0f) < epsilon);
        }


        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataCanParseFileWithMultipleGroups()
        {
            var inputString = "v -2.000000 -3.000000 -4.000000\n"
                + "g 2\n"
                + "f 1\n"
                + "v -3.000000 -3.000000 -4.000000\n"
                + "g 3\n"
                + "f 1";

            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData(inputString);

            Assert.True(parsedData.Count == 2);

            Assert.True(parsedData[0].Name == "2");
            Assert.True(parsedData[1].Name == "3");

            Assert.That(parsedData[0].Positions.Count == 1);
            Assert.That(parsedData[1].Positions.Count == 1);

            Assert.That(parsedData[0].Faces.Count == 1);
            Assert.That(parsedData[1].Faces.Count == 1);

            var vertex = parsedData[0].Positions[0];
            var epsilon = 0.001f;
            Assert.True((vertex.X - 2.0f) < epsilon);
            Assert.True((vertex.Y - 3.0f) < epsilon);
            Assert.True((vertex.Z - 4.0f) < epsilon);

            vertex = parsedData[1].Positions[0];
            Assert.True((vertex.X - 3.0f) < epsilon);
            Assert.True((vertex.Y - 3.0f) < epsilon);
            Assert.True((vertex.Z - 4.0f) < epsilon);
        }

        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataCanParseAValidNormalLine()
        {
            var inputString = "vn 0.000000 -1.000000 1.000000";

            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData(inputString);

            var parsedDataItem = parsedData[0];
            Assert.That(parsedDataItem.Normals.Count == 1);

            var normal = parsedDataItem.Normals[0];
            var epsilon = 0.001f;
            Assert.True((normal.X - 0.0f) < epsilon);
            Assert.True((normal.Y - -1.0f) < epsilon);
            Assert.True((normal.Z - 1.0f) < epsilon);
        }

        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataCanParseAValidTexCoordLine()
        {
            var inputString = "vt 0.000000 0.500000 1.000000";

            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData(inputString);

            var parsedDataItem = parsedData[0];
            Assert.That(parsedDataItem.TexCoords.Count == 1);

            var texCoord = parsedDataItem.TexCoords[0];
            var epsilon = 0.001f;
            Assert.True((texCoord.X - 0.0f) < epsilon);
            Assert.True((texCoord.Y - 0.5f) < epsilon);
            Assert.True((texCoord.Z - 1.0f) < epsilon);
        }

        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataCanParseATexCoordLineWihtTwoParameters()
        {
            var inputString = "vt 0.000000 0.500000";

            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData(inputString);

            var parsedDataItem = parsedData[0];
            Assert.That(parsedDataItem.TexCoords.Count == 1);

            var texCoord = parsedDataItem.TexCoords[0];
            var epsilon = 0.001f;
            Assert.True((texCoord.X - 0.0f) < epsilon);
            Assert.True((texCoord.Y - 0.5f) < epsilon);
            Assert.True((texCoord.Z - 0.0f) < epsilon);
        }

        [Test]
        [Category("OBJ Parser")]
        public void ParseOBJDataCanParseAValidFaceLine()
        {
            var inputString = "f 1/2/3 4/5/6 7/8";

            var loader = new OBJParser();
            var parsedData = loader.ParseOBJData(inputString);

            var parsedDataItem = parsedData[0];
            Assert.That(parsedDataItem.Faces.Count == 1);

            var face = parsedDataItem.Faces[0];
            Assert.AreEqual(3, face.Vertices.Count);

            var faceVertex = face.Vertices[0];
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(3, faceVertex.NormalIndex);
            Assert.AreEqual(2, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[1];
            Assert.AreEqual(4, faceVertex.PositionIndex);
            Assert.AreEqual(6, faceVertex.NormalIndex);
            Assert.AreEqual(5, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[2];
            Assert.AreEqual(7, faceVertex.PositionIndex);
            Assert.AreEqual(0, faceVertex.NormalIndex);
            Assert.AreEqual(8, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("FaceVertex Parser")]
        public void ParseFaceVertexStringParsesAStringWithPositionOnly()
        {
            var loader = new OBJParser();

            var inputString = "1";

            var faceVertex = loader.ParseFaceVertexString(inputString);
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(0, faceVertex.NormalIndex);
            Assert.AreEqual(0, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("FaceVertex Parser")]
        public void ParseFaceVertexStringParsesAStringWithPositionNormal()
        {
            var loader = new OBJParser();

            var inputString = "1//2";

            var faceVertex = loader.ParseFaceVertexString(inputString);
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(2, faceVertex.NormalIndex);
            Assert.AreEqual(0, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("FaceVertex Parser")]
        public void ParseFaceVertexStringParsesAStringWithPositionNormalTexCoord()
        {
            var loader = new OBJParser();

            var inputString = "1/3/2";

            var faceVertex = loader.ParseFaceVertexString(inputString);
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(2, faceVertex.NormalIndex);
            Assert.AreEqual(3, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("FaceVertex Parser")]
        public void ParseFaceVertexStringParsesAStringWithPositionTexCoord()
        {
            var loader = new OBJParser();

            var inputString = "1/3";

            var faceVertex = loader.ParseFaceVertexString(inputString);
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(0, faceVertex.NormalIndex);
            Assert.AreEqual(3, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("Face Parser")]
        public void ParseFaceReturnsAFaceWithZeroVerticesWhenStringIsEmpty()
        {
            var loader = new OBJParser();

            var inputString = "";

            var face = loader.ParseFace(inputString);
            Assert.AreEqual(0, face.Vertices.Count);
        }

        [Test]
        [Category("Face Parser")]
        public void ParseFaceCanHandleStringsWithIrregularWhiteSpacing()
        {
            var loader = new OBJParser();

            var inputString = " 1/2/3   4/5/6   7/8 ";

            var face = loader.ParseFace(inputString);
            Assert.AreEqual(3, face.Vertices.Count);

            var faceVertex = face.Vertices[0];
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(3, faceVertex.NormalIndex);
            Assert.AreEqual(2, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[1];
            Assert.AreEqual(4, faceVertex.PositionIndex);
            Assert.AreEqual(6, faceVertex.NormalIndex);
            Assert.AreEqual(5, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[2];
            Assert.AreEqual(7, faceVertex.PositionIndex);
            Assert.AreEqual(0, faceVertex.NormalIndex);
            Assert.AreEqual(8, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("Face Parser")]
        public void ParseFaceReturnsAFaceWithThreeVerticesWhenPassedAStringWithThreeVertexMappings()
        {
            var loader = new OBJParser();

            var inputString = "1/2/3 4/5/6 7/8";

            var face = loader.ParseFace(inputString);
            Assert.AreEqual(3, face.Vertices.Count);

            var faceVertex = face.Vertices[0];
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(3, faceVertex.NormalIndex);
            Assert.AreEqual(2, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[1];
            Assert.AreEqual(4, faceVertex.PositionIndex);
            Assert.AreEqual(6, faceVertex.NormalIndex);
            Assert.AreEqual(5, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[2];
            Assert.AreEqual(7, faceVertex.PositionIndex);
            Assert.AreEqual(0, faceVertex.NormalIndex);
            Assert.AreEqual(8, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("Face Parser")]
        public void ParseFaceReturnsAFaceWithFourVerticesWhenPassedAStringWithFourVertexMappings()
        {
            var loader = new OBJParser();

            var inputString = "1/2/3 4/5/6 7/8 9/10/11";

            var face = loader.ParseFace(inputString);
            Assert.AreEqual(4, face.Vertices.Count);

            var faceVertex = face.Vertices[0];
            Assert.AreEqual(1, faceVertex.PositionIndex);
            Assert.AreEqual(3, faceVertex.NormalIndex);
            Assert.AreEqual(2, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[1];
            Assert.AreEqual(4, faceVertex.PositionIndex);
            Assert.AreEqual(6, faceVertex.NormalIndex);
            Assert.AreEqual(5, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[2];
            Assert.AreEqual(7, faceVertex.PositionIndex);
            Assert.AreEqual(0, faceVertex.NormalIndex);
            Assert.AreEqual(8, faceVertex.TexCoordIndex);

            faceVertex = face.Vertices[3];
            Assert.AreEqual(9, faceVertex.PositionIndex);
            Assert.AreEqual(11, faceVertex.NormalIndex);
            Assert.AreEqual(10, faceVertex.TexCoordIndex);
        }

        [Test]
        [Category("VectorString Parser")]
        public void ParseVectorStringReturnsAVectorWithTheCorrectValuesWhenPassedAValidString()
        {
            var loader = new OBJParser();

            var inputString = "-2.000000 -3.000000 -4.000000";

            var vertex = loader.ParseVectorString(inputString);
            var epsilon = 0.001f;
            Assert.True((vertex.X - 2.0f) < epsilon);
            Assert.True((vertex.Y - 3.0f) < epsilon);
            Assert.True((vertex.Z - 4.0f) < epsilon);
        }

        [Test]
        [Category("VectorString Parser")]
        public void ParseVectorStringCanHandleStringsWithIrregularWhiteSpacing()
        {
            var loader = new OBJParser();

            var inputString = " -2.000000    -3.000000  -4.000000 ";

            var vertex = loader.ParseVectorString(inputString);
            var epsilon = 0.001f;
            Assert.True((vertex.X - 2.0f) < epsilon);
            Assert.True((vertex.Y - 3.0f) < epsilon);
            Assert.True((vertex.Z - 4.0f) < epsilon);
        }

    }
}
