﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using PBRInvestigation.Models.OBJLoader;
using SharpDX;

namespace PBRInvestigationTests
{

    [TestFixture]
    public class OBJProcessorTests
    {
        [Test]
        [Category("OBJ Processor")]
        public void ProcessReturnsAnEmptyInstanceWhenPassedEmptyData()
        {
            var parsedData = new ParsedOBJData();
            var parsedDataList = new List<ParsedOBJData>();
            parsedDataList.Add(parsedData);

            var processor = new OBJProcessor();

            var processedData = processor.Process(parsedDataList);

            Assert.True(processedData[0].VertexData.Count == 0);
            Assert.True(processedData[0].IndexData.Count == 0);
        }

        [Test]
        [Category("OBJ Processor")]
        public void ProcessReturnsAnEmptyInstanceWhenPassedParsedDataWithoutFaces()
        {
            var parsedData = new ParsedOBJData();
            parsedData.Positions.Add(new Vector3(1.0f, 1.0f, 1.0f));
            var parsedDataList = new List<ParsedOBJData>();
            parsedDataList.Add(parsedData);

            var processor = new OBJProcessor();

            var processedData = processor.Process(parsedDataList);

            Assert.True(processedData[0].VertexData.Count == 0);
            Assert.True(processedData[0].IndexData.Count == 0);
        }

        [Test]
        [Category ("OBJ Processor")]
        public void ProcessReturnsAnIndexAndVertexBufferContainingOneTriangleWithJustVertexDataWhenPassedCorrectData()
        {
            var parsedData = new ParsedOBJData();
            parsedData.Positions.Add(new Vector3(0.0f, 0.0f, 0.0f));
            parsedData.Positions.Add(new Vector3(1.0f, 1.0f, 0.0f));
            parsedData.Positions.Add(new Vector3(1.0f, 0.0f, 0.0f));

            var face = new Face();
            face.Vertices.Add(new FaceVertex(1));
            face.Vertices.Add(new FaceVertex(2));
            face.Vertices.Add(new FaceVertex(3));

            parsedData.Faces.Add(face);

            var parsedDataList = new List<ParsedOBJData>();
            parsedDataList.Add(parsedData);

            var processor = new OBJProcessor();

            var processedData = processor.Process(parsedDataList);

            Assert.True(processedData[0].VertexData.Count == 3);
            Assert.True(processedData[0].IndexData.Count == 3);

            Assert.True(VectorsAreEqual(processedData[0].VertexData[0].Position, new Vector3(0.0f, 0.0f, 0.0f), 0.001f));
            Assert.True(VectorsAreEqual(processedData[0].VertexData[1].Position, new Vector3(1.0f, 1.0f, 0.0f), 0.001f));
            Assert.True(VectorsAreEqual(processedData[0].VertexData[2].Position, new Vector3(1.0f, 0.0f, 0.0f), 0.001f));

            Assert.True(processedData[0].IndexData[0] == 0);
            Assert.True(processedData[0].IndexData[1] == 1);
            Assert.True(processedData[0].IndexData[2] == 2);
        }

        [Test]
        [Category("OBJ Processor")]
        public void ProcessInterpretsNegativeFaceIndicesAsTheInvertedIndexParseOrder()
        {
            var parsedData = new ParsedOBJData();
            parsedData.Positions.Add(new Vector3(0.0f, 0.0f, 0.0f));
            parsedData.Positions.Add(new Vector3(1.0f, 1.0f, 0.0f));
            parsedData.Positions.Add(new Vector3(1.0f, 0.0f, 0.0f));

            var face = new Face();
            face.Vertices.Add(new FaceVertex(-1));
            face.Vertices.Add(new FaceVertex(-2));
            face.Vertices.Add(new FaceVertex(-3));

            parsedData.Faces.Add(face);

            var parsedDataList = new List<ParsedOBJData>();
            parsedDataList.Add(parsedData);

            var processor = new OBJProcessor();

            var processedData = processor.Process(parsedDataList);

            Assert.True(processedData[0].VertexData.Count == 3);
            Assert.True(processedData[0].IndexData.Count == 3);

            Assert.True(VectorsAreEqual(processedData[0].VertexData[0].Position, new Vector3(1.0f, 0.0f, 0.0f), 0.001f));
            Assert.True(VectorsAreEqual(processedData[0].VertexData[1].Position, new Vector3(1.0f, 1.0f, 0.0f), 0.001f));
            Assert.True(VectorsAreEqual(processedData[0].VertexData[2].Position, new Vector3(0.0f, 0.0f, 0.0f), 0.001f));

            Assert.True(processedData[0].IndexData[0] == 0);
            Assert.True(processedData[0].IndexData[1] == 1);
            Assert.True(processedData[0].IndexData[2] == 2);
        }

        [Test]
        [Category ("OBJ Processor")]
        public void ProcessReturnsAnIndexAndVertexBufferContainingOneQuadWithJustVertexDataWhenPassedCorrectData()
        {
            var parsedData = new ParsedOBJData();
            parsedData.Positions.Add(new Vector3(0.0f, 0.0f, 0.0f));
            parsedData.Positions.Add(new Vector3(0.0f, 1.0f, 0.0f));
            parsedData.Positions.Add(new Vector3(1.0f, 1.0f, 0.0f));
            parsedData.Positions.Add(new Vector3(1.0f, 0.0f, 0.0f));
            
            var face = new Face();
            face.Vertices.Add(new FaceVertex(1));
            face.Vertices.Add(new FaceVertex(2));
            face.Vertices.Add(new FaceVertex(3));
            face.Vertices.Add(new FaceVertex(4));

            parsedData.Faces.Add(face);

            var parsedDataList = new List<ParsedOBJData>();
            parsedDataList.Add(parsedData);

            var processor = new OBJProcessor();
            var processedData = processor.Process(parsedDataList);

            Assert.True(processedData.Count == 1);

            Assert.True(processedData[0].VertexData.Count == 4);
            Assert.True(processedData[0].IndexData.Count == 6);

            Assert.True(VectorsAreEqual(processedData[0].VertexData[0].Position, new Vector3(0.0f, 0.0f, 0.0f), 0.001f));
            Assert.True(VectorsAreEqual(processedData[0].VertexData[1].Position, new Vector3(0.0f, 1.0f, 0.0f), 0.001f));
            Assert.True(VectorsAreEqual(processedData[0].VertexData[2].Position, new Vector3(1.0f, 1.0f, 0.0f), 0.001f));
            Assert.True(VectorsAreEqual(processedData[0].VertexData[3].Position, new Vector3(1.0f, 0.0f, 0.0f), 0.001f));

            Assert.True(processedData[0].IndexData[0] == 0);
            Assert.True(processedData[0].IndexData[1] == 1);
            Assert.True(processedData[0].IndexData[2] == 2);

            Assert.True(processedData[0].IndexData[3] == 0);
            Assert.True(processedData[0].IndexData[4] == 2);
            Assert.True(processedData[0].IndexData[5] == 3);
        }

        [Test]
        [Category ("OBJ Processor")]
        public void ProcessLoadsObjCubeAsExpected()
        {
            var parser = new OBJParser();
            var parsedData = parser.ParseOBJData(cubeObj);

            var processor = new OBJProcessor();
            var processedData = processor.Process(parsedData);

            Assert.True(processedData[0].VertexData.Count == 24);
            Assert.True(processedData[0].IndexData.Count == 36);
        }

        public bool VectorsAreEqual(Vector3 first, Vector3 second, float epsilon)
        {
            for(int i = 0; i < 3; i++)
            {
                if(Math.Abs(first[i] - second[i]) > epsilon)
                {
                    return false;
                }
            }

            return true;
        }

        private string cubeObj = "# cube.obj\n" +
                                "#\n" +
                                "\n" +
                                "o cube\n" +
                                "mtllib cube.mtl\n" +
                                "\n" +
                                "v -0.500000 -0.500000 0.500000\n" +
                                "v 0.500000 -0.500000 0.500000\n" +
                                "v -0.500000 0.500000 0.500000\n" +
                                "v 0.500000 0.500000 0.500000\n" +
                                "v -0.500000 0.500000 -0.500000\n" +
                                "v 0.500000 0.500000 -0.500000\n" +
                                "v -0.500000 -0.500000 -0.500000\n" +
                                "v 0.500000 -0.500000 -0.500000\n" +
                                "\n" +
                                "vt 0.000000 0.000000\n" +
                                "vt 1.000000 0.000000\n" +
                                "vt 0.000000 1.000000\n" +
                                "vt 1.000000 1.000000\n" +
                                "\n" +
                                "vn 0.000000 0.000000 1.000000\n" +
                                "vn 0.000000 1.000000 0.000000\n" +
                                "vn 0.000000 0.000000 -1.000000\n" +
                                "vn 0.000000 -1.000000 0.000000\n" +
                                "vn 1.000000 0.000000 0.000000\n" +
                                "vn -1.000000 0.000000 0.000000\n" +
                                "\n" +
                                "g cube\n" +
                                "usemtl cube\n" +
                                "s 1\n" +
                                "f 1/1/1 2/2/1 3/3/1\n" +
                                "f 3/3/1 2/2/1 4/4/1\n" +
                                "s 2\n" +
                                "f 3/1/2 4/2/2 5/3/2\n" +
                                "f 5/3/2 4/2/2 6/4/2\n" +
                                "s 3\n" +
                                "f 5/4/3 6/3/3 7/2/3\n" +
                                "f 7/2/3 6/3/3 8/1/3\n" +
                                "s 4\n" +
                                "f 7/1/4 8/2/4 1/3/4\n" +
                                "f 1/3/4 8/2/4 2/4/4\n" +
                                "s 5\n" +
                                "f 2/1/5 8/2/5 4/3/5\n" +
                                "f 4/3/5 8/2/5 6/4/5\n" +
                                "s 6\n" +
                                "f 7/1/6 1/2/6 5/3/6\n" +
                                "f 5/3/6 1/2/6 3/4/6\n";

    }
}
